@extends('layout.master')
@section('page_title')
    Franchise Request List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Franchise Request List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Report</span></li>
                <li><span>Franchise Request List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Franchise Request List</h2>
            </header>
        </section>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered"> 
                <tbody>
                    <tr>
                        <td>
                            <button type="button" id="clk" class="btn btn-primary">Excel</button>
                        </td>
                    </tr>
                </tbody>
            </table><br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                        <tr>
                            <th>Lead ID</th>
                            <th>Created Date</th>
                            <th>Request Date</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>                      
                            <th>Tell Caller</th>
                            <th>Present Stage</th>
                         </tr>
                    </thead>
                    <tbody>
                        @if(count($result) > 0)
                            @foreach ($result as $key => $val)
                                <tr>
                                    <td>
                                        {{ $val->reference_id }}
                                    </td>
                                    <td>
                                        {{ $val->created_date }}
                                    </td>
                                    <td>
                                        {{ $val->requestdate }}
                                    </td>
                                    <td>
                                        {{ $val->name }}
                                    </td>   
                                    <td>
                                        {{ $val->email }}
                                    </td>
                                    <td>
                                        {{ $val->mobile }}
                                    </td>
                                    <td>
                                        {{ $val->tellcallername }}
                                    </td>       
                                    <td>
                                        <?php echo config('custom.fracnhsie_stage.'.$val->stage_id);?>
                                    </td>                           
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class ="pull-right" style="margin-bottom:20px">
                {!! $result->render() !!}
            </div>
        </div>
    </section>
@endsection
@section('page_level_script_bottom')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#loading').hide();
        });

        $('#clk').click(function(){
            window.location.href="{{ route('franchiserequestlistexport') }}";
        })
    </script>
@endsection			