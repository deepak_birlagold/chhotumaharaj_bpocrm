@extends('layout.master')
@section('page_title')
    Distributor Not Interested
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Distributor Not Interested</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Distributor Not Interested</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-4">Distributor Not Interested</h2>
                    <div class="alert alert-success col-md-5 message" style="padding:7px;margin-bottom: -8px;display:none;"></div>
                </div>
            </header>
            <div class="panel-body">
                <form class="navbar-form" role="search" action="{{ route('recevied-list') }}" name="frm" id="frm">
                    <div class="row">
                        <label class="col-md-1 control-label">Search</label> 
                        <div class="col-md-3 form-group">
                            <input type="text" name='q' value="{{ $q }}" class="form-control" id="q">
                        </div>
                       <div class="form-group col-md-3">
                            <select class="form-control" name="option" id="option">
                                <option value=""> -- Select Status -- </option>
                                <option value="ID" @if($option=='ID') selected @endif>ID</option>
                                <option value="Name" @if($option=='Name') selected @endif>Name</option>
                                <option value="Email" @if($option=='Email')selected @endif>Email</option>                           
                                <option value="Mobile" @if($option=='Mobile') selected @endif>Mobile</option>
                            </select>   
                        </div>
                        <div class="form-group col-md-3">
                            <input type="submit" class="btn" value="Search">
                            <a href="{{ route('recevied-list') }}" class="btn btn-primary" >Clear</a>
                        </div>
                    </div>													
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Tele Caller</th>
                            </tr>
                        </thead>
                        <tbody>                  
                            @if(count($leadsourcevalue) > 0)
                                @foreach ($leadsourcevalue as $key => $val)
                                    <tr data-id="{{ $key }}">
                                        <td> 
                                            {{ $val->leadid }}
                                        </td>
                                        <td>
                                            {{ $val->leadname }}
                                        </td>
                                        <td>
                                            {{ $val->leadmobile }}
                                        </td>
                                        <td>
                                            {{ $val->leademail }}
                                        </td>
                                        <td>
                                            {{ $val->username }}
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" align="center">
                                        Not Data Found
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class ="pull-right" style="margin-bottom:20px">
                    {!! $leadsourcevalue->appends(['q'=>(isset($q)?$q:""),'option'=>(isset($option)?$option:"")])->render() !!}
                </div>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::to('/public/assets/javascripts/app.js')}}"></script>
    <script src="{{URL::to('/public/assets/bootbox/bootbox.min.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide(); 
        });
        
        
        $('#frm').submit(function(){
            $('#loading').show();
            var search = $('#q').val()=="" ? $('#q').focus() : true;
            var option = $('#option option:selected').val()=="" ? $('#option').focus() : true;
            if(search==true && option==true){
                return true;
            }
            $('#loading').hide();
            return false;
        });

</script>

@endsection			