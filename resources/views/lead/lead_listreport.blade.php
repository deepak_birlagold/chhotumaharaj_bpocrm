@extends('layout.master')
@section('page_title')
User List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">User List </a>
</li>
@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Lead List  Report</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Lead List Report</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
</section>


<section role="main" class="content-body">
    <!-- start: page -->
    <section class="panel">

        <div class="panel-body">

            <table class="table table-bordered table-striped mb-none" id="datatable-editable">

                <tr>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                <form class="form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-2 control-label">Date range</label>
                        <div class="col-md-6">
                            <div class="input-daterange input-group" data-plugin-datepicker="">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control" name="startdata" value="{{isset($startdata)?$startdata:""}}">
                                <span class="input-group-addon">to</span> 
                                <input type="text" class="form-control" name="enddate" value="{{isset($enddate)?$enddate:""}}">

                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <button type="submit" class="btn btn-primary" >Submit</button>
                            <a href="{{ route('leadlist_report') }}" class="btn btn-primary" >Clear</a>
                             <a href="#" class="btn btn-primary">Download</a>
                        </div>

                       
                    </div>
                </form></div>

                </tr>


                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th class="hidden-phone">Mobile</th>
                        <th class="hidden-phone">State</th>
                        <th class="hidden-phone">City</th>
                        <th class="hidden-phone">Status</th>
                        <!--th class="hidden-phone"> Action</th-->
                    </tr>
                </thead>
                <tbody>                  
                <tbody>
                    <?php
                    if (count($leadsourcevalue) > 0) {
                        foreach ($leadsourcevalue as $key => $val) {
                            ?>
                            <tr class="gradeX">

                                <td> <?php echo $val['reference_id']; ?>  </td>
                                <td>
                                    <?php echo $val['name']; ?> 
                                </td>
                                <td>
                                    <?php echo $val['email']; ?> 
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val['mobile']; ?> 
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val['state_name']; ?>  
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val['city_name']; ?>  
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val['status']; ?>  
                                </td>


                                <?php
                            }
                        } else {
                            ?>

                        </tr>
                        <tr>
                            <td colspan="8" align="center">
                                Not Data Found
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </section>
</section>


@endsection
@section('page_level_script_bottom')


@endsection			