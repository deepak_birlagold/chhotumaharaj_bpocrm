@extends('layout.master')
@section('page_title')
    Add New Lead
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Add Lead</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Add Lead</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-5">Add Lead</h2>
                    @if($errors->any())
                        <div class="alert alert-danger text-center col-md-3">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </header>
            <div class="panel-body">
                <form id="form" action="{{ route('leadedit') }}" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="statename" value="" id="statename">
                    <input type="hidden" name="cityname" value="" id="cityname">
                    <input type="hidden" name="reference_id" value="{{ request()->segment(2) }}">
                    <input type="hidden" name="_token" id="_tokensc" value="{{ csrf_token() }}">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label"> Name <span class="required">*</span></label>
                            <div class="col-md-6">
                                 <input type="text" name="name" class="form-control" placeholder="Please Enter Mobile"  value="{{ $leadvalue['name'] }}"/>     
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Mobile <span class="required">*</span></label>
                            <div class="col-md-6">

                                <input type="text" name="mobile" class="form-control" placeholder="Please Enter Mobile" pattern="[789][0-9]{9}" value="{{ $leadvalue['mobile'] }}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-6">                              
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input type="email" name="email" class="form-control" placeholder="Please Enter Email" autocomplete="off" value="{{ $leadvalue['email'] }}" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Gender</label>
                            <div class="col-md-6">
                                <select class="form-control" name="gender" id="gender">
                                    <option value=""> -- Select Gender --</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="not_to_say">Prefer Not to say</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">State <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" name="state" id="state">
                                    <option value="">Select State</option>
                                    @foreach($states as $key=>$val)
                                        <option value="{{$val['id'] }}">{{$val['state_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">City <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" name="city" id="city">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                                <label class="col-md-3 control-label">Area <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="area" id="area">
                                        <option value="">Select Area</option>
                                    </select>
                                </div>
                            </div>
                            
                        <div class="form-group">
                            <label class="col-md-3 control-label">Address <span class="required">*</span></label>
                            <div class="col-md-6">
                                <textarea style="width:100%;" class="form-control" rows="3" name="address" data-error-container="#editor1_error" placeholder="Please Enter Address" ></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Landmark <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="landmark" data-required="1" class="form-control" placeholder="Please Enter Landmark" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pincode </label>
                            <div class="col-md-6">
                                <input type="text" name="pincode" data-required="1" class="form-control" placeholder="Please Enter Pincode" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Company Name</label>
                            <div class="col-md-6">
                                <input type="text" name="company_name" data-required="1" class="form-control" placeholder="Please Enter Company Name" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Income </label>
                            <div class="col-md-6">
                                <input type="text" name="income" data-required="1" class="form-control" placeholder="Please Enter Income" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Lead Source</label>
                            <div class="col-md-6">
                                <select class="form-control" name="leadsourcevalue" id="leadsourcevalue">
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($leadsourcevalue as $val) {
                                        ?>
                                        <option value="<?php echo $val->leadsource_id ?>" <?php if ($leadvalue['lead_source'] == $val->leadsource_id) echo 'selected'; ?>> <?php echo $val->leadsource_name ?></option>
                                    <?php } ?>

                                </select>
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <button type="submit" class="btn green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ URL::to('lead-list')}}" class="btn btn-default" >Cancel</a>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </footer>
                </form>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            App.init();
            FormValidation.init();
            $('#state').trigger('change');
            $('#loading').hide();
        });

    $("#state").change(function () {
        var stateID = $("#state").val();
        var token = $("#_tokensc").val();
        $('#statename').val($("#state option:selected").html());
        $("#city option").not(':first').remove();
        $("#area option").not(':first').remove();
        if (stateID) {
            $.ajax({
                url: "{{route('getcitylist')}}",
                data: {'stateid': stateID, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#city").append(
                            $("<option></option>").text(obj.city_name).val(obj.id)
                        );
                    });
                },
                error: function () {
                    alert("Server is Busy!!");
                },
                complete: function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });

     $('#city').change(function(){
        $('#cityname').val($('#city option:selected').html());
        var token = $("#_tokensc").val();
        var cityId = $("#city").val();
        $("#area option").not(':first').remove();
        if (cityId) {
            $.ajax({
                url: "{{route('getarealist')}}",
                data: {'cityid': cityId, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                  $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#area").append(
                            $("<option></option>").text(obj.area_name).val(obj.id)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });

    $('#city').change(function () {
        $('#cityname').val($('#city option:selected').html());
    });

    function formsubmit(obj){
        var flag = false;
        var mobile = $('input[name="mobile"]').val();
        $.ajax({
            url: "{{route('check-mobile')}}",
            data: {'mobile': mobile,'lead_id':"{{ request()->segment(2) }}"},
            type: 'get',
            async: true,
            cache: false,
            clearForm: false,
            beforeSend:function(){  
              $('#loading').show();
            },
            success:function (response) {
                if(response== undefined){
                    var check = function(){
                        callback();
                    };
                }
                if(response==1){
                    alert("Mobile No Already Exists!!!");
                }else{
                    obj.submit();
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
        });
        return false;
    }

    function callback(){
        return check();
    }
</script>

@endsection
