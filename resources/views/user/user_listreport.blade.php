
@extends('layout.master')
@section('page_title')
User List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">User List </a>
</li>
@endsection
@section('content')


<!-- end: page -->

<section role="main" class="content-body">
    <header class="page-header">
        <h2>User List</h2>

        @if(Session::has('flash_message'))
        <div class="alert alert-success text-cente ">
            {{ Session::get('flash_message') }}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger text-cente">
            @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
        </div>
        @endif

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>User</span></li>
                <li><span>User List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <!-- start: page -->
    <section class="panel">
        <header class="panel-heading">
            <!--  <div class="panel-actions">
                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                 <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
             </div>
            -->
            <h2 class="panel-title">User List</h2>
        </header>
        <div class="panel-body">

            <table class="table table-bordered table-striped mb-none" id="datatable-editable">

                <tr>
                <form class="form-horizontal">
                    <div class="form-group col-md-3">
                        <label class="sr-only" for="exampleInputUsername2">Username</label>
                        <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="{{isset($q)?$q:""}}">
                    </div>

                    <div class="form-group col-md-3">
                       <select class="form-control" name="search_option" required="" >
                               <option value="ID" <?php if ($search_option == 'ID') echo 'selected'; ?> >ID</option>
                            <option value="Name" <?php if ($search_option == 'Name') echo 'selected'; ?>>Name</option>
                            <option value="Email" <?php if ($search_option == 'Email') echo 'selected'; ?>>Email</option>                           
                            <option value="Mobile" <?php if ($search_option == 'Mobile') echo 'selected'; ?>>Mobile</option>
                            <option value="Status"<?php if ($search_option == 'Status') echo 'selected'; ?>>Status</option>

                            </select> 
  
                    </div>
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
               

                    <a class="btn btn-primary" href="{{ URL::route('user_list',['q'=>$q,'search_option'=>$search_option,'flag'=>1])}}">Download Report</a>
               
                </tr>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th class="hidden-phone">Mobile</th>
                        <th class="hidden-phone">Role</th>
                        <th class="hidden-phone">Reporting Manager</th>

                        <!-- <th class="hidden-phone"> Action</th> -->
                    </tr>
                </thead>
                <tbody>


<?php
if (count($data) > 0) {
    foreach ($data as $key => $val) {
        ?>
                            <tr class="gradeX">

                                <td>
                            <?php echo $val->id; ?>
                                </td>
                                <td>
                                    <?php echo empty($val->name) ? "-" : $val->name; ?>
                                </td>
                                <td>
                                    <?php echo $val->email; ?> 
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val->mobile; ?> 
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val->role; ?>  
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val->remanager; ?>  
                                </td>
                                <!-- <td class="center hidden-phone"> 
                                    {{--@if(in_array(Auth::user()->role,['Relationship Manager','Customer Service','Trainee']))--}}
                                    <!-- <a href="{{ URL::to('edit_user',$val->id)}}" class="btn btn-primary">Edit</a>  -->
                                    {{--@endif--}}
                                <!-- </td> -->
    <?php
    }
} else {
    ?>

                        </tr>
                        <tr>
                            <td colspan="8" align="center">
                                Not Data Found
                            </td>
                        </tr>
<?php } ?>
                </tbody>
            </table>
        </div>
    </section>
    <!-- end: page -->
</section>


@endsection
@section('page_level_script_bottom')
<!-- Vendor -->
<!-- Examples -->

<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

@endsection			