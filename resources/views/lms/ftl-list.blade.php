@extends('layout.master')
@section('page_title')
    LMS FTL Assign FO
@endsection
@section('page_level_style_top')

	<header class="page-header">
        <h2>Assign FO</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>FTL Assign FO</span></li>
                <li><span>Assign FO</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')

<section role="main" class="content-body">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Lead List</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped mb-none">
				<thead>
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Lead Name</th>
						<th>Appointment Date</th>
						<th>FO</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $count=1 ;?>
					@foreach($results as $key=>$data)
						<tr>
							<td>
								{{ $key+1 }}
							</td>
							<td>
								{{ $data['reference_id']}}
							</td>
							<td>
								{{$data['name']}}
							</td>
							<td>
								{{ date('d M ,Y H:i',strtotime($data['appointment_date'])) }}
							</td>
							<td>
								<select class="form-control foassogn" id="sel{{ $data['reference_id'] }}">
									<option value="">Select FO</option>
								</select>
							</td>
							<td>
								<a data-ref="{{ $data['reference_id'] }}" class="assign btn btn-primary">Assign</a>
							</td>
						</tr>
						<?php $count++;?>
					@endforeach
				</tbody>
			</table>
			<div class ="pull-right" style="margin-bottom:20px">
				
			</div>
		</div>
	</section>
</section>
@endsection
@section('page_level_script_bottom')

	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<script src="{{URL::to('assets/scripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
	<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	<script>
		var ftldata= {!! json_encode($user) !!};
		jQuery(document).ready(function() { 
			$('#loading').hide();
	   		$('.pagination li a').addClass('clk');
	   		$.each(ftldata,function(index,item){
	   			$(".foassogn").append(
	        		$("<option></option>").text(item).val(index)
	      		);
	    	});
		});

		$('.assign').click(function(){
			var selid = $(this).data('ref');
			var fo = $('#sel'+selid+' option:selected').val();
			if(fo==""){
				alert("Select FO!!!!");
				return false;
			}
			var ref = $(this).data('ref');
			$.ajax({
				url: "{{ route('assign-fo') }}",
				data:{'ref_id':ref,'fo_id':fo,'_token':"{{ csrf_token() }}"},  
				type: 'post',
				async:false,
				cache: false,
				beforeSend:function(){	
					$('#loading').show();
				},
				success:function(response){
					window.location.reload();
				},
				error:function(){
					alert("Server is Busy!!");
				},
				complete:function (data) {
	    			$('#loading').hide();
	    		}
			});
		});
    </script>
@endsection
