@extends('layout.master')
@section('page_title')
Lms Report
@endsection
@section('breadcrumb')
<li>
    <a href="#">LMS Report</a>
</li>
@endsection

@section('content')
<header class="page-header">
    <h2>Dashboard</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
            <li><span>Lms Report</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">Lms Report</h2>
                @if($errors->any())
                <div class="alert alert-danger text-center col-md-3">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">
            
        <a  class="btn btn-primary pull-right excel">Excel Export</a>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr id="tbl">
                            <th>
                                Id
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Entry Date
                            </th>
                            <th>
                                Relationship Manager
                            </th>
                            <th>
                                Appointment Date
                            </th>
                            <th>
                                Appointment Time
                            </th>
                            <th>
                                TL Name
                            </th>
                            <th>
                                TL Verifier Date
                            </th>
                            <th>
                                TL Verifier Time
                            </th>
                            <th>
                                Verifier Name
                            </th>
                            <th>
                                Verifier Date
                            </th>
                            <th>
                                Verifier Time
                            </th>
                            <th>
                                Stage
                            </th>
                        </tr>
                    </thead>

                    <tbody id="tbl">
                        @foreach($results as $data)
                        <?php
                            $rmname = '-';
                            $tlname = '-';
                            $type = "";
                            if (empty($data->customer_id) || $data->customer_id == 0) {
                                $type = "Lead";
                                if (isset($user[$data->rel_manager_id])) {
                                    $rmname = $user[$data->rel_manager_id]['name'];
                                    if ($user[$data->rel_manager_id]['reporting_manager']) {
                                        $tlname = $user[$user[$data->rel_manager_id]['reporting_manager']]['name'];
                                    }
                                }
                            }
                        ?>

                        <tr>
                            <td>
                                {{$data->reference_id}}
                            </td>
                            <td>
                                {{$data->name}}
                            </td>
                            <td>
                                {{$type}}
                            </td>
                            <td>
                                {{ $data->created }}
                            </td>
                            <td>
                                {{ $rmname }}
                            </td>

                            <td>
                                {{ $data->appointdate }}
                            </td>
                            <td>
                                {{ $data->appointtime }}
                            </td>
                            <td>
                                {{ $tlname }}

                            </td>
                            <td>
                                --

                            </td>
                            <td>
                                --
                            </td>
                            <td>
                                --
                            </td>
                            <td>
                                {{ $data->verifierdate }} 
                            </td>
                            <td>

                                @if($data->verifiertime)
                                {{ date('H:i',strtotime('+5 hours +30 minutes', strtotime($data->verifiertime))) }}
                                @endif

                            </td>
                            <td>

                                {{$data->stage_id  }} {{$data->stage_remarks}}

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class ="pull-right" style="margin-bottom:20px">
            {!! $results->appends(request()->except('page'))->links() !!}
        </div>
    </section></section>

@endsection

@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script>

jQuery(document).ready(function () {
    $('#loading').hide();
    App.init();    
});
$('.excel').click(function () {
    window.location.href = "{{ route('lms-report-excel') }}?" + $('#frm').serialize();
})
</script>
@endsection