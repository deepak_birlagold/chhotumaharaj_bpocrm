@extends('layout.master')
@section('page_title')
Spok Deatils
@endsection
@section('page_level_style_top')
<header class="page-header">
    <h2>Spok Deatils</h2>
    <link rel="stylesheet" type="text/css" href="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Spok Deatils</span></li>
            <li><span>Spok Management </span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

@endsection
@section('content')
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">Spok Deatils</h2>
                @if($errors->any())
                <div class="alert alert-danger text-center col-md-3">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">
            <form action="{{route('spok-update')}}" id="spokupdate" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="fo_type" id="fo_type">
                <input type="hidden" name="ftl_id" id="ftl_id">
                <input type="hidden" name="lateflag" id="lateflag" value={{ request()->flag }}>
                       <input type="hidden" name="lateappoinment" id="lateappoinment" value="{{$reference->appointment_date}}">
                <div class="form-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Name:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="name" data-required="1" class="form-control" placeholder="Please Enter Name" value="{{$reference->name}}" readonly/>
                                    </div>
                                    <label class="control-label col-md-2">Mobile:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="mobile" data-required="1" class="form-control" placeholder="Please Enter Mobile Number" value="{{$reference->mobile}}" readonly />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">State:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="name" data-required="1" class="form-control" placeholder="Please Enter Name" value="{{$area['state'] or '' }}" readonly/>
                                    </div>
                                    <label class="control-label col-md-2">City:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="name" data-required="1" class="form-control" placeholder="Please Enter Name" value="{{$area['city'] or '' }}" readonly/>
                                    </div>
                                </div>
                            </div>									
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-11">
                                        <textarea type="textarea" rows="5" cols="10" class="form-control" readonly>{{ key(json_decode($reference->verifier,1)['address']) }}</textarea>
                                        Address
                                    </div>	
                                </div>
                            </div>
                        </div>


                        <div style="border:none;" class="portlet box green">
                            <div class="portlet box greens ">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="fa fa-phone-square" style="font-size: 20px;"></i> Spok Interaction </div>
                                </div>
                                <div class="portlet-body form">
                                    <input type="hidden" name="id" value="{{$reference->reference_id}}">
                                    <div class="form-body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label">Call Status</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="call_status" id="call_status" onchange="getSubStatus();">
                                                                <option value=""> --- Select Call Status ---</option>
                                                                @foreach($call_status as $key=>$val)
                                                                <option value="{{$val['id']}}">{{$val['name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label">Call Sub Status</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="call_sub_status" id="call_sub_status" >
                                                                <option value=""> --- Select Call Sub Status ---</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display:none;" id="userfolist">
                                                        <label class="col-md-6 control-label">FO/FTL List</label>
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="sel_fo" id="sel_fo">
                                                                <option value=""> --- Select FO ---</option>
                                                                @foreach($folist as $key=>$val)
                                                                <option value="{{$val['id']}}" data-reproting="{{ $val['reporting_manager'] }}">{{$val['name']}} ({{$val['role']}})</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6 control-label">Appointment DateTime </label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="appointment_date" id="appointmentdate" class="form-control" data-required="1" readonly placeholder="Please Select Appointment Date" value="{{date('d-m-Y H:i:s',strtotime($reference->appointment_date))}}" />
                                                        </div>
                                                    </div>                                                        
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label"> Interaction Summary </label>
                                                        <div class="col-md-8">
                                                            <textarea type="textarea" rows="6" name="verification_interaction" class="form-control" value="" placeholder="Please Enter Verification Interaction " style="resize:vertical;" ></textarea>	
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions fluid">
                        <div class="col-md-offset-4 col-md-6">
                            <button type="submit" class="btn green" id="clk">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{{ URL::to('verifier-list')}}" class="btn btn-default" >Cancel</a>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">	
                        </div>
                    </div>	
                </div>
            </form>
        </div>
    </section>
</section>

<div class="modal fade" id="remarks_interaction" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title text-purple"><b>Remarks</b></h4>
            </div>
            <div class="modal-body" >
                <p id="remarkinteraction"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<a href="../../../../bpocrm/resources/views/lms/tl-edit-verifier.blade.php"></a>
@endsection
@section('page_level_script_bottom')

<script type="text/javascript" src="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>



<script>
                                                                var datevalue = "{{ $reference->appointment_date }}";
                                                                jQuery(document).ready(function () {
                                                                    $('#loading').hide();
                                                                    App.init();

                                                                    FormValidation.init();
                                                                });
                                                                var d = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
                                                                $('#appointmentdate').datetimepicker({
                                                                    format: "dd-mm-yyyy hh:ii:ss",
                                                                    //startDate: d,
                                                                });

                                                                function getSubStatus() {
                                                                    statusId = $("#call_status").val();
                                                                    $("#call_sub_status option").remove();
                                                                            var callSubStatus = {!! json_encode($call_sub_status) !!}
                                                                    ;
                                                                    $("#call_sub_status").append($("<option></option>").text('').val(''));
                                                                    $.each(callSubStatus, function (index, item) {
                                                                        if (item.verifier_call_status_id == statusId && item.status == 'active') {
                                                                            $("#call_sub_status").append(
                                                                                    $("<option></option>").text(item.name).val(item.id)
                                                                                    );
                                                                        }
                                                                    });
                                                                }

                                                                var ftl = {!! json_encode(array_column($folist, 'role', 'id')) !!}
                                                                ;
                                                                $('#call_sub_status').change(function () {
                                                                    var value = $('#call_sub_status option:selected').val();
                                                                    $('#userfolist').hide();
                                                                    $('#sel_fo,#fo_type,#ftl_id').val('');
                                                                    $("#appointmentdate").datetimepicker("remove");
                                                                    $('#appointmentdate').val("{{date('d-m-Y H:i:s',strtotime($reference->appointment_date))}}")
                                                                    if (value != "") {
                                                                        if (value == "5") {
                                                                            $('#userfolist').show();
                                                                        } else if (value == "7") {
                                                                            setdate();
                                                                        }
                                                                    }
                                                                });
                                                                $('#sel_fo').change(function () {
                                                                    $('#fo_type').val(ftl[$('#sel_fo option:selected').val()]);
                                                                    $('#ftl_id').val($('#sel_fo option:selected').data('reproting'));
                                                                })
</script>



@endsection
