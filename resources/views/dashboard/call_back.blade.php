
@extends('layout.master')
@section('page_title')
User List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">Call Back  List </a>
</li>
@endsection
@section('content')


<!-- end: page -->

<section role="main" class="content-body">
    <header class="page-header">
        <h2>User List</h2>

        @if(Session::has('flash_message'))
        <div class="alert alert-success text-cente ">
            {{ Session::get('flash_message') }}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger text-cente">
            @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
        </div>
        @endif

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>User</span></li>
                <li><span>User List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <!-- start: page -->
    <section class="panel">
        <header class="panel-heading">
            <!--  <div class="panel-actions">
                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                 <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
             </div>
            -->
            <h2 class="panel-title">User List</h2>
        </header>
        <div class="panel-body">

            <table class="table table-bordered table-striped mb-none" id="datatable-editable">

                <tr>
                <form class="form-horizontal">
                    <div class="form-group col-md-3">
                        <label class="sr-only" for="exampleInputUsername2">Username</label>
                        <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search">
                    </div>

                    <div class="form-group col-md-3">
                        <select class="form-control" name="search_option" required="" >


                        </select> 

                    </div>
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>


                <a class="btn btn-primary" href="">Download Report</a>

                </tr>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
<!--                        <th>Email</th>-->
<!--                        <th class="hidden-phone">Mobile</th>-->
<!--                        <th class="hidden-phone">Role</th>-->
                        <th class="hidden-phone">Reporting Manager</th>

                        <th class="hidden-phone"> Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>                     
                        <?php
                     
                        if (count($leadcountdata) > 0) {
                            foreach ($leadcountdata as $key => $val) {

//                             echo 'eee'.$val['name'];
//                        echo "<pre>";
//                        print_r($leadcountdata);
//                        echo "</pre>";
//                        die();   
                                ?>
                            <tr class="gradeX">

                                <td>1</td>
                                <td>
                                    <?php echo empty($val['leadcr']) ? "-" : $val['leadcr']; ?>
                                </td>
                                <td>
                                    <?php echo $val['remanager']; ?>  
                                </td>
<!--                                <td class="center hidden-phone">
                                    <?php //echo $val['mobile']; ?> 
                                </td>-->
                               
                                <td class="center hidden-phone">
                                   active
                                </td>
                               
                               
                                <?php
                            }
                        } else {
                            ?>

                        </tr>
                        <tr>
                            <td colspan="8" align="center">
                                Not Data Found
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>

                </tr>

                </tbody>
            </table>
        </div>
    </section>
    <!-- end: page -->
</section>


@endsection
@section('page_level_script_bottom')
<!-- Vendor -->
<!-- Examples -->

<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

@endsection			