@extends('layout.master')
@section('page_title')
    Add New Franchise Lead
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Add Franchise Lead</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Add Franchise Lead</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-5">Franchise Lead</h2>
                    @if($errors->any())
                        <div class="alert alert-danger text-center col-md-3">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </header>
            <div class="panel-body">
                <form id="franchiseLeadForm" action="{{ route('savefranchise') }}" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="_token" id="_tokensc" value="{{ csrf_token() }}">
                    <input type="hidden" name="state_name" value="" id="statename">
                    <input type="hidden" name="city_name" value="" id="cityname">
                    <input type="hidden" name="areaname" value="" id="areaname">
                    <div class="form-group">
                        <label class="col-md-4 control-label"> Name <span class="required">*</span></label>
                        <div class="col-md-5">
                            <input type="text" name="name" class="form-control" placeholder="Please Enter Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Email</label>
                        <div class="col-md-5">  
                            <input type="email" name="email" class="form-control" placeholder="Please Enter Email"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Mobile <span class="required">*</span></label>
                        <div class="col-md-5">
                            <input type="number" id="mobile" name="mobile" class="form-control" placeholder="Please Enter Mobile" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Alternate Mobile</label>
                        <div class="col-md-5">
                            <input type="number" name="alternate_no" class="form-control" placeholder="Please Enter Alternate Mobile"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">State <span class="required">*</span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="state_id" id="state">
                                <option value="">Select State</option>
                                @foreach($states as $key=>$val)
                                <option value="{{$val['id'] }}">{{$val['state_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">City <span class="required">*</span></label>
                        <div class="col-md-5">
                           <select class="form-control" name="city_id" id="city">
                                <option value="">Select City</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Area <span class="required">*</span></label>
                        <div class="col-md-5">
                            <input type="text" name="area_name" class="form-control" placeholder="Please Enter Email"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Address<span class="required">*</span></label>
                        <div class="col-md-5">
                            <textarea type="text" name="address" class="form-control" placeholder="Please Enter Address"/></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Pincode</label>
                        <div class="col-md-5">
                            <input type="text" name="pincode" class="form-control" placeholder="Please Enter Email"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Preferred Location</label>
                        <div class="col-md-5">
                            <textarea name="preffered_locations" class="form-control" placeholder="Please Enter Preferred Location"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="inputDefault">How did you find out about 'CHHOTU MAHARAJ' - Cine Restaurant Franchise?</label>
                        <div class="col-md-5">
                            <select class="form-control mb-md" name="source" id="source">
                                <option value="null">---</option>
                                <option value="self">Self Distributor</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row source" style="display: none;">  
                        <label class="col-md-4 control-label" for="inputDefault">Please Specify</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="other_source" id="other_source">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="inputDefault">Do you have any existing property/land suitable for 'CHHOTU MAHARAJ' - Cine Restaurant setup ready? </label>
                        <div class="col-md-5">
                            <select class="form-control mb-md" name="existing_property" id="existing_property">
                                <option value="0">---</option>
                                <option value="1">Yes</option>
                                <option value="2">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row existing_property" style="display: none;">   
                        <label class="col-md-4 control-label" for="inputDefault">Details of Property/Land (in sq. ft.)</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="details_of_property" id="details_of_property">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label" for="inputDefault">Do you have ready finance for this project?</label>
                        <div class="col-md-5">
                            <select class="form-control mb-md" name="ready_finance_for_project" id="ready_finance_for_project">
                                <option value="0">---</option>
                                <option value="1">Yes</option>
                                <option value="2">No</option>
                            </select>
                        </div>
                    </div>
                    <!--if yes open a tab please specify-->
                    <div class="form-group row ready_finance_for_project" style="display: none;">   
                        <label class="col-md-4 control-label" for="inputDefault">How much ready finance for this project?</label>
                        <div class="col-md-5">
                            <select class="form-control mb-md" name="how_much_finance" id="how_much_finance">
                                <option value="null">---</option>
                                <option>10-15 lakhs</option>
                                <option>15-20 lakhs</option>
                                <option>20-25 laksh</option>
                                <option>25-30 lakhs</option>
                                <option>30 lakhs & Above</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 control-label" for="inputDefault">Are you looking for finance/loan? </label>
                        <div class="col-md-5">
                            <select class="form-control mb-md" name="looking_for_finance" id="looking_for_finance">
                                <option value="0">---</option>
                                <option value="1">Yes</option>
                                <option value="2">No</option>
                            </select>
                        </div>
                    </div>
                    <!--if yes open a tab please specify-->
                    <div class="form-group row looking_for_finance" style="display: none;"> 
                        <label class="col-md-4 control-label" for="inputDefault">Please mention your desired finance loan amount in lakhs.</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="desired_finance_loan_amount" id="desired_finance_loan_amount">
                        </div>
                    </div>

                    <div class="form-group row">    
                        <label class="col-md-4 control-label" for="inputDefault">Please tell us your reason for taking up a 'CHHOTU MAHARAJ' - Cine Restaurant Franchisee.</label>
                        <div class="col-md-5">
                            <textarea class="form-control" rows="3" id="textareaDefault" name="reason_for_franchise"></textarea>
                        </div>
                    </div>
                   
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <button type="submit" id ="submit" class="btn btn-primary text-color-light text-uppercase font-weight-semibold outline-none custom-btn-style-2 custom-border-radius-1 submit" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                               
                            </div>
                        </div>
                    </footer>
                </form>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
    jQuery(document).ready(function () {
        App.init();
        FormValidation.init();
        $('#loading').hide();
    });
    $('#submit').click(function(e){
        if($('#franchiseLeadForm').valid()==false){
            return false;
        }
        var mobile=$("#mobile").val();
        var flag = false;
        $('#loading').show();
        $.ajax({
            url: "{{route('franchisemobile-check')}}",
            data: {'mobile': mobile, '_token': "{{ csrf_token() }}"},
            type: 'post',
            async: false,
            cache: false,
            clearForm: false,
            success: function (response) {
                if(response==1){
                    alert("Mobile  Already Exist!!!!");
                    $('#loading').hide();
                }else{
                    flag = true;
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                
            }
        });
        return flag;
    });
    $("#state").change(function () {
        var stateID = $("#state").val();
        var token = $("#_tokensc").val();
        $('#statename').val($("#state option:selected").html());
        $("#city option").not(':first').remove();
        $("#area option").not(':first').remove();
        if (stateID) {
            $.ajax({
                url: "{{route('getcitylist')}}",
                data: {'stateid': stateID, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                  $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#city").append(
                            $("<option></option>").text(obj.city_name).val(obj.id)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });

    $('#city').change(function(){
        $('#cityname').val($('#city option:selected').html());
        var token = $("#_tokensc").val();
        var cityId = $("#city").val();
        $("#area option").not(':first').remove();
        if (cityId) {
            $.ajax({
                url: "{{route('getarealist')}}",
                data: {'cityid': cityId, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                  $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#area").append(
                            $("<option></option>").text(obj.area_name).val(obj.id)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });

    $('#source').change(function() {
        if($('#source').val() == 'Other') {
            $('.source').show();
        } else {
            $('#other_source').val('');
            $('.source').hide();
        }
    });

    $('#existing_property').change(function() {
        if($(this).val() == '1') {
            $('.existing_property').show();
        } else {
            $('#details_of_property').val('');
            $('.existing_property').hide();
        }
    });

    $('#ready_finance_for_project').change(function() {
        if($(this).val() == '1') {
            $('.ready_finance_for_project').show();
        } else {
            $('#how_much_finance').val('');
            $('.ready_finance_for_project').hide();
        }
    });

    $('#looking_for_finance').change(function() {
        if($(this).val() == '1') {
            $('.looking_for_finance').show();
        } else {
            $('#desired_finance_loan_amount').val('');
            $('.looking_for_finance').hide();
        }
    });
   
</script>

@endsection
