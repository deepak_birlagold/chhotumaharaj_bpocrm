<?php
return [ 'ROLES' => array('manager' => 'Manager','assistant_manager' =>'Assistant Manager','team_lead'=>'Team Lead','tele_caller'=>'Tele Caller'/*,'Verifier Head'=>'Verifier Head','Verifier'=>'Verifier','Ftl'=>'Ftl','FO'=>'FO','Voc'=>'Voc' ,'Spoc'=>'Spoc'*/),
	'url'=>'http://www.cherishgold.com/crm/api/',
	'BpoCrmKey'=>['call'=>'callstatusandsubstatus','bpo'=>'bpo','state'=>'state','city'=>'city','area'=>'area'],
	'timecache'=>240,
	'urlimage'=>'http://www.cherishgold.com/crm/sp-laravel/public/',
	'userhierarchy'=>['Manager'=>'Admin','Assistant Manager'=>'Manager','Team Lead'=>'Assistant Manager','Tele Caller'=>'Team Lead'/*,'Verifier Head'=>'Admin','Verifier'=>'Verifier Head','Spoc'=>'Admin','Ftl'=>'Admin','FO'=>'Ftl','Voc'=>'Admin'*/],
	'selfurl'=>'http://www.cherishgold.com/',
	'weburl' => 'http://www.cherishgold.com/',
	'FoPayment'=>[1=>'Online',4=>'Cash Received',2=>'Cheque Received',10=>'Paytm',6=>'NEFT/RTGS'],
	'niremarks'=>['financialissue'=>'Financial Issue','productunsatisfied'=>'Product Unsatisfied','unconvinced'=>'Unconvinced','other'=>'Others'],
	'Met'=>['saledone'=>'Sale Done','followup'=>'Follow Up','callback'=>'Call Back','notint'=>'Not Interested'],
	'Notmet'=>['notcontactable'=>'Not Contactable','fieldissue'=>'Postpond By field'],
	'Metkey'=>['saledone','followup','callback','notint'],
	'current_proffession'=>[1=>'CA',2=>'Lawyer'],
	'crmApiUrl'=>'http://10.0.0.235:8000/git_updated/crm/api/',
	'fracnhsie_stage'=>[0=>'Franchise Added',1=>'Interested',2=>'Aggreement File Uploaded',3=>'Accountent Accepted',4=>'Accountent Rejected',5=>'Admin Verified',6=>'Admin Rejected'],
	'parentUrl'=>'http://localhost/git_updated/chhotumaharaj-crm/'
];?>
