<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class SmsSentDetail extends Authenticatable {

    public $table = "sms_sent_details";
    protected $fillable = [
        'user_id', 'bpo_id', 'lead_id', 'mobile_no'
    ];
    public $timestamps = true;
    public $primaryKey = "id";
}
