<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model{
    
    protected $primaryKey = 'id';
    public $table = "email_template";
}  
?>