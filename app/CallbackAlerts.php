<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallbackAlerts extends Model
{
	protected $connection = 'mysql';
    protected $primaryKey = 'id';
    public $table="callback_alerts";
	public $timestamps = false;
}
