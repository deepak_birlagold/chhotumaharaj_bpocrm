<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyLink extends Model
{
    protected $primaryKey = 'id';
    public $table="verify_link";
	public $timestamps = true;
}
