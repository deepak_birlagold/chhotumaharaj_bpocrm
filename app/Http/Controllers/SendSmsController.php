<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SendSmsController extends Controller
{
	private $api_url;
        private $time;
        private $unicode;
        private $working_key;
        private $start;
        private $sender_id;
        private $api_method;
        public $api;
        public $wk;
        public $sid;
        public $to;
        public $method;
		
		function setMethod($method) {
            $this->api_method = $method;
        }

		function setWorkingKey($wk) {
            $this->working_key = $wk;
        }

        function setSenderId($sid) {
            $this->sender_id = $sid;
        }
		
		function setapiurl($apiurl) {

            $this->api = $apiurl;
            $a = strtolower(substr($apiurl, 0, 7));

            if ($a == "http://") { //checking if already contains http://
                $api_url = substr($apiurl, 7, strlen($apiurl));
                $this->api_url = $api_url;
                $this->start = "http://";
            } elseif ($a == "https:/") { //checking if already contains htps://
                $api_url = substr($apiurl, 8, strlen($apiurl));
                $this->api_url = $api_url;
                $this->start = "https://";
            } else {
                $this->api_url = $apiurl;
                $this->start = "http://";
            }
        }

       function __construct($apiurl, $method, $wk, $sd) {
            $this->setMethod($method);
            $this->setWorkingKey($wk);
            $this->setSenderId($sd);
            $this->setapiurl($apiurl);
        }

        function send_sms($to, $message, $dlr_url, $type = "xml") {
            return $this->process_sms($to, $message, $dlr_url, $type = "xml", $time = "null", $unicode = "null");
        }

        
        function schedule_sms($to, $message, $dlr_url, $type = "xml", $time) {
            $this->process_sms($to, $message, $dlr_url, $type = "xml", $time, $unicode = '');
        }

       
        function unicode_sms($to, $message, $dlr_url, $type = "xml", $unicode) {
            $this->process_sms($to, $message, $dlr_url, $type = "xml", $time = '', $unicode);
        }

       
        function process_sms($to, $message, $dlr_url = "", $type = "xml", $time = '', $unicode = '') {
            $message = urlencode($message);
            $dlr_url = urlencode($dlr_url);
            $this->to = $to;
            $to = substr($to, -10);
            $arrayto = array("9", "8", "7");
            $to_check = substr($to, 0, 1);

            if (in_array($to_check, $arrayto))
                $this->to = $to;
            else
                echo "invalid number";

            if ($time == 'null')
                $time = '';
            else
                $time = urlencode($time);
            $time = "&time=$time";
            if ($unicode == 'null')
                $unicode = '';
            else
                $unicode = "&unicode=$unicode";

            $url = "$this->start$this->api_url/index.php?method=$this->api_method&api_key=$this->working_key&sender=$this->sender_id&to=$to&message=$message&format=$type&dlr_url=$dlr_url$time$unicode";
            return $this->execute($url);
        }

        
        function messagedelivery_status($mid) {
            $url = "$this->start$this->api_url/index.php?method=sms.status&api_key=$this->working_key&id=$mid&format=xml";
          return $this->execute($url);
        }

      
        function groupdelivery_status($gid) {
            $url = "$this->start$this->api_url/index.php?method=sms.groupstatus&api_key=$this->working_key&groupid=$gid&format=xml";
            $this->execute($url);
        }

        
        function execute($url) {
            $ch = curl_init();
            // curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);
			return $output;
        }

    }    

