<?php
namespace App\Http\Controllers;
use Mail;
use App\Interactions;
use App\User;
use App\FranchiseLead;
use App\SmsSentDetail;					  
use App\CallbackAlerts;
use DateTime;
use Validator;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use App\VerifierInteraction;
use DB;
use Session;
use App\EmailTemplate;
use App\States;	  

class FranchiseController extends Controller {


    public function index(){
        $states = getstate();
        $data['controller'] = "franchise";
        $data['action'] = "franchiseadd";
        $data['states'] = $states;
        return view('franchise/franchise-create', $data);
    }


    public function franchisemobilecheck(Request $request){
        $mobile = $request['mobile'];
        $mobile = FranchiseLead::where('mobile', $mobile)
                ->pluck('mobile', 'reference_id');
        $check = json_decode(json_encode($mobile), 1);
        if (count($check) > 0) {
            return 1;
        }
        return 0;
    }

    public function createfranchise(Request $request){
        $dataReq = ['customer_status'=>'Draft','lead_source'=>'rm','user_id'=>Auth::user()->id,'bpo_id'=>Auth::user()->bpo_id];
        if((Auth::user()->role=="Tele Caller") || (Auth::user()->role == "admin" && Auth::user()->status == "inactive") || Session::get('admin_self_rm') == 1) {
            $dataReq['rel_manager_id'] = Auth::user()->id;
        }
        $request->merge($dataReq);
        if(Session::has('user_id')){
            $request->merge(['crm_user_id'=>Session::get('user_id')]);
        }
        $data = FranchiseLead::create($request->all());
        if($data){
            if((Auth::user()->role=="Tele Caller") || (Auth::user()->role == "admin" && Auth::user()->status == "inactive") || Session::get('admin_self_rm') == 1){
                return redirect()->route('franchsie-details',[$data->reference_id]);
            }
            return redirect()->route('unassigned-franchiselead');    
        }else{
            return back();
        }
        
    }

	public function franhciseleadList(Request $request, $sep = null){
		$arrsearch = array('option'=>$request['option'],'q'=>$request['q']);
        $role = Auth::user()->role;
        $user_id = Auth::user()->id;    
        $dataunion = DB::table("franchise_leads")
                       ->join("users","id",'=','rel_manager_id')
                       ->leftjoin("interactions","interactions.interaction_id",'=','franchise_leads.last_interaction_id')
                       ->where("franchise_leads.status",'In Progress')
                       ->where("franchise_leads.bpo_id", Auth::user()->bpo_id)
                       ->whereNotNull('rel_manager_id')
                       ->where(function($q) use ($role,$user_id){
                            if($role=='Team Lead'){
                                $q->where("reporting_manager",$user_id);
                            }elseif($role=='Tele Caller'){
                                $q->where("rel_manager_id",$user_id);
                            }
                        })
                        ->where(function ($query) use ($arrsearch) {
                              if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('franchise_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('franchise_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('franchise_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('franchise_leads.email', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Status')
                                    $query = $query->where('franchise_leads.status', 'LIKE', "%$search%");
                            }
                        })
                        ->select("franchise_leads.reference_id", "franchise_leads.name", "franchise_leads.city_name", "franchise_leads.mobile", "franchise_leads.state_name", "franchise_leads.email", "franchise_leads.created_at", DB::raw("Null as applicant_name"), DB::raw("'Lead' as type"), "franchise_leads.rel_manager_id", "franchise_leads.customer_status", "franchise_leads.status","users.name as username","interactions.call_substatus_id","interactions.call_status_id")
                        ->orderBy('reference_id','desc')
                        ->paginate(10);
		$data['result'] = $dataunion;
        $data['controller'] = 'franchise';
        $data['action'] = 'franchsielead-list';
		return view('franchise.franchise-list',$data)->with($arrsearch);
	}
    public function franchiseDetails($id) {
        $result = DB::table("franchise_leads")
                ->where('bpo_id',Auth::user()->bpo_id)
                ->where("franchise_leads.reference_id", '=',$id)
                ->where('franchise_leads.status','In Progress')
                ->select("franchise_leads.appointment_date", "franchise_leads.appointment_time", "franchise_leads.verifier", "franchise_leads.reference_id", "franchise_leads.mobile", "franchise_leads.name", "franchise_leads.email", "franchise_leads.created_at", "franchise_leads.status", "franchise_leads.reference_id", "franchise_leads.json_struct", "franchise_leads.converted_customer_id", "franchise_leads.customer_status","franchise_leads.stage_id", "franchise_leads.stage_remarks","rel_manager_id")
                  ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
       
        $data['reference_data'] = json_decode(json_encode($result), 1);
        $data['callstatus'] = CallStatusSubStatus();
        $interaction = Interactions::where('reference_id', '=', $id)
                                   ->where('user_type','franchise-lead')
                ->orderBy('created_at', 'desc')
                ->get();
        $data['interaction'] = json_decode(json_encode($interaction), 1);
        $user = User::where('bpo_id', Auth::user()->bpo_id)
                ->lists('name', 'id');
        $data['rms'] = json_decode(json_encode($user), 1);
        $emailtemplate = EmailTemplate::where('status','active')
                                      ->select('title','id')
                                      ->get();
        $data['emailtemplate'] = json_decode(json_encode($emailtemplate),1);
        $data['controller'] = 'franchise';
        $data['action'] = 'franchsielead-list';
        return view('franchise/franchise-details', $data);
    }
    public function postfracnhiseInteraction(Request $request) {
        $data = array();
        if ($request->isMethod('post')) {
            $callstatusname = callstatus();
            $callsubstatusname = callsubstatus();
            $rms = User::lists('name', 'id');
            $data = array();

            $dispcallback = $callbackdate = null;
            if ($request->nextdatetime != ""){
                $callbackdate = date("Y-m-d h:i:s", strtotime($request->nextdatetime));
                $dispcallback = date("M d, Y H:i",strtotime($request->nextdatetime));
            }
            $interaction = new Interactions;
            $interaction->user_type = $request->user_type;
            $interaction->reference_id = $request->reference_id;
            $interaction->bpo_id = Auth::user()->bpo_id;
            $interaction->rm_id = Auth::user()->id;
            $interaction->call_initiator = $request->callinitiator;
            $interaction->call_status_id = $request->callstatus;
            $interaction->call_substatus_id = $request->callsubstatus;
            $interaction->call_substatus_other = $request->callsubstatusother;
            $interaction->callback_datetime = $callbackdate;
            $interaction->remarks = $request->interaction;
            $interaction->save();
            $flagdelete = DB::table('callback_alerts')
                            ->where('customer_id', '=', $request->reference_id)
                            ->whereIn('user_type', ['franchise'])
                            ->delete();
            $sessiondata = $request->session()->get('notification') - $flagdelete;
            $request->session()->put('notification', $sessiondata);

            if ($request->nextdatetime != "" && $request->callsubstatus != "66") {
                $refdata = FranchiseLead::where([['reference_id', $request->reference_id], ['bpo_id', Auth::user()->bpo_id]])->get()->toArray();
                
                $objcallback = new CallbackAlerts();
                $objcallback->customer_id = $request->reference_id;
                $objcallback->customer_name = $refdata[0]['name'];
                ;
                $objcallback->customer_mobile = $refdata[0]['mobile'];
                ;
                $objcallback->callbacktime = date("Y-m-d h:i:s", strtotime($request->nextdatetime));
                $objcallback->relationship_manager = Auth::user()->id;
                $objcallback->call_status_id = $request->callstatus;
                $objcallback->call_sub_status_id = $request->callsubstatus;
                $objcallback->user_type = 'franchise';
                $objcallback->bpo_id = Auth::user()->bpo_id;
                $objcallback->save();
                $dataconversion = date("Y-m-d", strtotime($request->nextdatetime));
                if (strtotime($dataconversion) == strtotime(date('Y-m-d'))) {
                    $sessiondata = $request->session()->get('notification') + 1;
                    $request->session()->put('notification', $sessiondata);
                }
            }
            $intarray = ['last_interaction_id'=>$interaction->interaction_id];
            if($request->callsubstatus=="79"){
                $intarray = array_merge($intarray,['status'=>'Received','request_date'=>date('Y-m-d')]);
                $data['refresh'] = 1;    
            }
            $update_lead = FranchiseLead::where('reference_id', $request->reference_id)
                     ->update($intarray);
            $data['interaction'] = array('datetime' => date("M d, Y H:i", strtotime($interaction->created_at)),
                'Logged_By' => $rms[$interaction->rm_id],
                'Remarks' => $interaction->remarks,
                'Call_Initiator' => $interaction->call_initiator,
                'Call_Status' => $callstatusname[$request->callstatus],
                'Call_Sub_Status' => $callsubstatusname[$request->callsubstatus],
                'Other' => $interaction->call_substatus_other,
                'Call_Back' => empty($dispcallback) ? "" : $dispcallback,
            );
            $data['return'] = true;
        }
        return $data;
    }
	public function franchiseleadinterestedlist(Request $request) {
        $arrsearch=['q'=>$request['q'],'option'=>$request['option']];
		$result = DB::table("franchise_leads")
                    ->join("users", "id", '=', 'rel_manager_id')
                    ->where("franchise_leads.status", 'In progress')
                    ->where("franchise_leads.bpo_id", Auth::user()->bpo_id)
                    ->select("franchise_leads.name as leadname", "franchise_leads.mobile as leadmobile", "franchise_leads.email as leademail","franchise_leads.reference_id as leadid","users.name as username")
                    ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('franchise_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('franchise_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('franchise_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('franchise_leads.email', 'LIKE', "%$search%");
                            }
                        })
                    ->paginate(20);
        $data['controller'] = "notinterested";
        $data['action'] = 'notinterestedfranchiselist';
        $data['leadsourcevalue'] = $result;
        return view('franchise.franchise-notinterested-list', $data)->with($arrsearch);
    }
    public function franchisemarknotinterested(Request $request){
        $id = $request['id'];
        return FranchiseLead::where('reference_id',$id)
                    ->update(['status'=>'Not Interested']);

    }

    public function franchisemarkrecevied(Request $request){
        $id = $request['id'];
        return FranchiseLead::where('reference_id',$id)
                    ->update(['status'=>'Received']);

    }
    public function leadchangerm(Request $request) {
        $rmid = $request['rm_id'];
        if(!$request->ajax()){
            //if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
            $ref_id=$request->session()->get('ref_value');
            $lead_id = array_reduce($ref_id, 'array_merge', array());
            $this->changerm($lead_id,$rmid);
            return redirect()->back()->with(['messages'=>'RM Change Successfully!!']);
        }else{
            $lead_id = [$request['lead_id']];
            return $this->changerm($lead_id,$rmid);
        }
        

    }
    public function franchiseleadchangermlist(Request $request) {
        $arrsearch = ['q'=>$request['q'],'option'=>$request['option']];
                                          
        $role = Auth::user()->role;
        if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
        }
        $tl = User::where('bpo_id',Auth::user()->bpo_id)
                  ->where('status','active');
        if($role=='Assistant Manager'){
            $tl = $tl->where('reporting_manager',Auth::user()->id);
        }elseif($role=='Team Lead'){
            $tl = $tl->where('id',Auth::user()->id);
        }
        $tl = $tl->where('role','Team Lead')
                 ->pluck('id');
        $user = DB::table('users as a1')
                  ->join('users as a2','a1.reporting_manager','=','a2.id')
                  ->where('a1.role','Tele Caller')
                  ->where('a1.bpo_id',Auth::user()->bpo_id)
                  ->whereIn('a1.reporting_manager',$tl)
                  ->where('a1.status','active')
                  ->select('a1.name as tellcallername','a1.id as tellcallerid','a2.name as tlname')
                  ->get();
        $data['reporting'] = json_decode(json_encode($user),1);

        $result = DB::table("franchise_leads")
                    ->join("users", "id",'=','rel_manager_id')
                    ->where("franchise_leads.status", 'In progress')
                    ->where("franchise_leads.bpo_id",Auth::user()->bpo_id)
                    ->where(function ($query) use ($arrsearch) {
                        if (!empty($arrsearch['option'])) {
                            $search = $arrsearch['q'];
                            if ($arrsearch['option'] == 'ID')
                                $query = $query->where('franchise_leads.reference_id', '=', "$search");
                            else if ($arrsearch['option'] == 'Name')
                                $query = $query->where('franchise_leads.name', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Mobile')
                                $query = $query->where('franchise_leads.mobile', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Email')
                                $query = $query->where('franchise_leads.email', 'LIKE', "%$search%");
                        }
                    })
                    ->select("franchise_leads.name as leadname", "franchise_leads.mobile as leadmobile", "franchise_leads.email as leademail", "franchise_leads.rel_manager_id as rel_manager_id","franchise_leads.reference_id as leadid", "users.name as username")
                    ->paginate(5);
        $data['controller'] = "reportc";
        $data['action'] = 'franchiseleadchangerm';
        $data['leadsourcevalue'] = $result;
        return view('franchise/franchiselead-changerm',$data)->with($arrsearch);
    }

    public function franchiseleadchangerm(Request $request) {
        $rmid = $request['rm_id'];
        if(!$request->ajax()){
            //if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
            $ref_id=$request->session()->get('ref_value');
            $lead_id = array_reduce($ref_id, 'array_merge', array());
            $this->changerm($lead_id,$rmid);
            return redirect()->back()->with(['messages'=>'RM Change Successfully!!']);
        }else{
            $lead_id = [$request['lead_id']];
            return $this->changerm($lead_id,$rmid);
        }

    }

    public function changerm($lead,$rm){
        return FranchiseLead::whereIn('reference_id',$lead)
                   ->update(['rel_manager_id'=>$rm]);
    }
}