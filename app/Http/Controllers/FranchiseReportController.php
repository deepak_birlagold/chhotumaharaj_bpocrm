<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;

class FranchiseReportController extends Controller{

    public function franchiseleadlistreportdata(Request $request) {
		$arrchk = $request->all();
        $result = $this->leaddata($request)->paginate(10);
        $data['controller'] = "franchisereport";
        $data['action']='franchiselead';
        $data['leadsourcevalue'] = $result;
        return view('franchisereport/franchiselead-listreport',$data)->with($arrchk);
    }

    public function leaddata(Request $request){
        $arrchk = $request->all();
        return DB::table("franchise_leads")
                	->leftjoin("users","id",'=','rel_manager_id')
                    ->leftjoin("interactions","interactions.interaction_id",'=','franchise_leads.last_interaction_id')
                   	//->leftjoin("lead_source","lead_source.leadsource_id",'=','franchise_leads.lead_source')
                   	->where("franchise_leads.status",'In progress')
                    ->where("franchise_leads.bpo_id",Auth::user()->bpo_id)
                    ->where(function($qry) use ($arrchk){
                        if(!empty($arrchk['fromdate']) || !empty($arrchk['todate'])){
                            $obj=DB::raw("date(franchise_leads.created_at)");
                            if((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))){
                                $qry=$qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])),date("Y-m-d", strtotime($arrchk['todate']))]);
                            } elseif((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))){
                                $qry=$qry->where($obj,">=" ,[date("Y-m-d", strtotime($arrchk['fromdate']))]);
                            } elseif((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))){
                                $qry=$qry->where($obj,"<=",[date("Y-m-d", strtotime($arrchk['todate']))]);
                            }
                        }
                    })
                    ->select("franchise_leads.name as leadname","franchise_leads.mobile as leadmobile","franchise_leads.email as leademail", "state_name","city_name","franchise_leads.reference_id as leadid","users.name as username","interactions.call_substatus_id","interactions.call_status_id","franchise_leads.created_at as leadcrated","interactions.created_at as interactioncreated",'interactions.remarks');
        
    }

    public function franchiseleadreportexport(Request $request){
    	$result = $this->leaddata($request)->get();
    	$data = json_decode(json_encode($result),1);
    	$filename = "FranchiseLead-data";
    	$call_status = callstatus();
        $sub_call_status= callsubstatus();
        Excel::create($filename.date("d-m-Y"), function($excel){
            $excel->sheet('Sheet 1', function($sheet) {});
        })->store('csv',storage_path('excel'));
        $header = ['Name','Mobile','Email','State','City','Lead ID','Lead Source','Tele Caller','Call Status','Call Sub Status','Lead Created','Call Date','Remarks'];
        Excel::load('storage/excel/'.$filename.date("d-m-Y").'.csv',function($doc) use ($data,$header,$filename,$call_status,$sub_call_status){
            $string='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $sheet = $doc->setActiveSheetIndex(0);
            for($i=0;$i<count($header);$i++){
                $sheet->setCellValue($string[$i].'1',$header[$i]);
            }
            $append=1;
            for($j=0;$j<count($data);$j++){
                $append=$append+1;
                $i=0;
                $sitedata = $data[$j];
                $callstatus = !empty($sitedata['call_status_id']) ? $call_status[$sitedata['call_status_id']] : "";
                $callsubstatus = !empty($sitedata['call_substatus_id']) ? $sub_call_status[$sitedata['call_substatus_id']] : "";
                $arr = array_merge($sitedata,['call_status_id'=>$callstatus,'call_substatus_id'=>$callsubstatus]);
                foreach($arr as $key=>$value){
                    $disp= $value;
                    $sheet->setCellValue($string[$i].$append,$disp);
                    $i++;
                }
            }
            unlink(storage_path('excel/'.$filename.date("d-m-Y").'.csv'));
        })->download('csv');
    }	

    public function convertedfranchiselead(Request $request){
        $data['result'] = $this->convertedleadreport($request)->paginate(50);
        $data['controller'] = "franchisereport";
        $data['action'] = "franchiseconverted";
        return view('franchisereport.franchiselead-converted',$data);
    }

    public function convertedleadreport(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        return  DB::table("franchise_leads")
                  ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                  ->where('franchise_leads.bpo_id', $bpo_id)
                  ->where('franchise_leads.status', 'Converted')
                  ->where('franchise_leads.customer_status','Verified')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("franchise_leads.reference_id",DB::raw("DATE_FORMAT(franchise_leads.created_at,'%d-%m-%Y') as created_date"),DB::raw("DATE_FORMAT(franchise_leads.request_date,'%d-%m-%Y') as requestdate"),DB::raw("DATE_FORMAT(franchise_leads.converted_date,'%d-%m-%Y') as conversiondate"),DB::raw("DATE_FORMAT(franchise_leads.request_date,'%d-%m-%Y') as verifieddate"),"franchise_leads.name","franchise_leads.email","franchise_leads.mobile","users.name as tellcallername",DB::raw("DATE_FORMAT(franchise_leads.verified_date,'%d-%m-%Y') as verified_date"))
                  ->orderBy('reference_id','desc');
    }

    public function franchiseconvertedexport(Request $request){
        $data = json_decode(json_encode($this->convertedleadreport($request)->get()),1);
        $header = ['Lead ID','Created','Request Date','Coversion Date','Verified Date','Name','Email','Mobile','Telle Caller','Verification Date'];
        return $this->createexcel("Converted",$data,$header);
    }

    public function franchiserequestlead(Request $request){
        $data['result'] = $this->requestleadreport($request)->paginate(50);
        $data['controller'] = "franchisereport";
        $data['action'] = "franchiserequest";
        return view('franchisereport.franchise-request',$data);
    }

    public function requestleadreport(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("franchise_leads")
                  ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                  ->where('franchise_leads.bpo_id', $bpo_id)
                  ->where('franchise_leads.status', 'Received')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("franchise_leads.reference_id",DB::raw("DATE_FORMAT(franchise_leads.created_at,'%d-%m-%Y') as created_date"),DB::raw("DATE_FORMAT(franchise_leads.request_date,'%d-%m-%Y') as requestdate"),"franchise_leads.name","franchise_leads.email","franchise_leads.mobile","users.name as tellcallername","franchise_leads.stage_id")
                  ->orderBy('reference_id','desc');
    }

    public function franchiserequestexport(Request $request){
        $data = json_decode(json_encode($this->requestleadreport($request)->get()),1);
        $header = ['Lead ID','Created','Request Date','Name','Email','Mobile','Telle Caller','Stage'];
        foreach ($data as $key => $value) {
            $data[$key]['stage_id']= config('custom.fracnhsie_stage.'.$value['stage_id']);
        }
        return $this->createexcel("Request",$data,$header);
    }

    public function verificationlead(Request $request){
        $data['result'] = $this->verificationleadreport($request)->paginate(50);
        $data['controller'] = "franchisereport";
        $data['action'] = "verification";
        return view('report.lead-verification',$data);
    }

    public function verificationleadreport(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("distributor_leads")
                  ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                  ->where('distributor_leads.bpo_id', $bpo_id)
                  ->where('distributor_leads.status', 'Converted')
                  ->where('distributor_leads.customer_status','Active')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("distributor_leads.reference_id",DB::raw("DATE_FORMAT(distributor_leads.created_at,'%d-%m-%Y') as created_date"),"distributor_leads.name","distributor_leads.email","distributor_leads.mobile","users.name as tellcallername","emiamount")
                  ->orderBy('reference_id','desc');
    }

    public function verificationexport(Request $request){
        $data = json_decode(json_encode($this->verificationleadreport($request)->get()),1);
        $header = ['Lead ID','Created','Name','Email','Mobile','Telle Caller','EMI Amount'];
        return $this->createexcel("Verification",$data,$header);
    }

    public function notinterestedfranchisereport(Request $request){
        $data['result'] = $this->notinterested($request)->paginate(50);
        $data['controller'] = "franchisereport";
        $data['action'] = "notinterestedfranchise";
        return view('franchisereport.franchiselead-notinterested',$data);
    }

    public function notinterested(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("franchise_leads")
                  ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                  ->where('franchise_leads.bpo_id', $bpo_id)
                  ->where('franchise_leads.status', 'Not Interested')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("franchise_leads.reference_id",DB::raw("DATE_FORMAT(franchise_leads.created_at,'%d-%m-%Y') as created_date"),"franchise_leads.name","franchise_leads.email","franchise_leads.mobile","users.name as tellcallername")
                  ->orderBy('reference_id','desc');
    }

    public function franchisenotinterestedexport(Request $request){
        $data = json_decode(json_encode($this->notinterested($request)->get()),1);
        $header = ['Lead ID','Created','Name','Email','Mobile','Telle Caller'];
        return $this->createexcel("NotInterested",$data,$header);
    }

    public function lead_interaction_report(Request $request) {
        $finaldata = array();
        $data = array();
        $searchemail = $request['searchemail'];
        if (isset($request['searchemail'])) {
            $searchemail = $request['searchemail'];
            $loginbpoid = Auth::user()->bpo_id;
            $distributor_leadstatushistory = Lead::where([['distributor_leads.email', 'like', '%' . $searchemail . '%'], ['distributor_leads.bpo_id', $loginbpoid]])
                    ->select(['distributor_leads.reference_id', 'distributor_leads.name', 'distributor_leads.email', 'distributor_leads.mobile', 'distributor_leads.state_name', 'distributor_leads.city_name', 'interactions.call_initiator', 'interactions.call_status_id', 'interactions.call_substatus_id', 'interactions.call_substatus_other', 'interactions.callback_datetime', 'interactions.remarks'])
                    ->leftjoin('interactions', 'distributor_leads.reference_id', '=', 'interactions.reference_id')
                    ->orderBy('distributor_leads.email')
                    ->get();
            $data = json_decode(json_encode($distributor_leadstatushistory), 1);

            foreach ($data as $key => $val) {
                $finaldata[$val['reference_id']][] = $val;
            }
        }
        $result = ['status' => 1, 'msg' => '', 'data' => $finaldata];
        $iswith = true;
        $iswithdata = array('searchemail' => $searchemail, 'controller' => 'lead', 'action' => 'lead_status_history');
        $viewFileName = "lead.lead_interaction_report";

        if ($request['flag'] == 1) {
            $header = ["Id", "Name", "Email", "Mobile", "State", "City", "Call Initiator", "Call Status", "Call Sub Status", "Call Sub Status(Other)", "Callback Time", "Remarks"];
            $this->createexcel("LeadInteraction", $data, $header);
            exit;
        }
        return view($viewFileName, $result)->with($iswithdata);
    }
}
