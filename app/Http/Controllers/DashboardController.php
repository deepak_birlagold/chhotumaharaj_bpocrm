<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;

class DashboardController extends Controller {

    public function dashboard() {
        $received = 'received';
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        $datasales = DB::table("distributor_leads")
                        ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                        ->where('distributor_leads.bpo_id', $bpo_id)
                        ->where('distributor_leads.status', 'Converted')
                        ->where('distributor_leads.customer_status','Verified')
                        ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                        })
                        ->select(DB::raw('count(reference_id) as totalconverted') )
                        ->first();
        $resultsales = json_decode(json_encode($datasales), 1);
        $data['converted'] = $resultsales['totalconverted'];
        
        
        $datalead = DB::table("distributor_leads")
                      ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                      ->where('distributor_leads.bpo_id',$bpo_id)
                      ->where('distributor_leads.status', 'In Progress')
                      ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                      })
                      ->select(DB::raw('count(distributor_leads.reference_id) as totallead'))
                        ->first();
        $resultlead = json_decode(json_encode($datalead), 1);
        $data['lead'] = $resultlead['totallead'];
        
        $datanotintersted = DB::table("distributor_leads")
                              ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                              ->where('distributor_leads.bpo_id',$bpo_id)
                              ->where('distributor_leads.status', 'Not Interested')
                              ->where(function($query) use ($role, $report_manager) {
                                if ($role == 'Team Lead') {
                                    $query = $query->where('users.reporting_manager', '=', " $report_manager");
                                } else if ($role == 'Tele Caller') {
                                    $query = $query->where('users.id', '=', " $report_manager");
                                } elseif ($role == 'Assistant Manager') {
                                    $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                                }
                            })
                            ->select(DB::raw('count(distributor_leads.reference_id) as totallead'))
                            ->first();
        $resultnotintersted = json_decode(json_encode($datanotintersted), 1);
        $data['notintersted'] = $resultnotintersted['totallead'];
       

        $datareceive = DB::table("distributor_leads")
                         ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                         ->where('distributor_leads.bpo_id', $bpo_id)
                         ->where('distributor_leads.status', 'Received')
                         //->where('distributor_leads.customer_status', 'Request')
                         ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }   
                       })
                       ->select(DB::raw('count(distributor_leads.reference_id) as totallead'))
                       ->first();
        $resultreceive = json_decode(json_encode($datareceive), 1);
        $data['leadreceive'] = $resultreceive['totallead'];


        $datacallback = $this->callback_detail("distributor")
                              ->select(DB::raw('count(callback_alerts.id) as callbackdata'))
                              ->first();
        $resultcallback = json_decode(json_encode($datacallback), 1);
        $data['distributorcallback'] = $resultcallback['callbackdata'];


        $datasalesfranchise = DB::table("franchise_leads")
                        ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                        ->where('franchise_leads.bpo_id', $bpo_id)
                        ->where('franchise_leads.status', 'Converted')
                        ->where('franchise_leads.customer_status','Verified')
                        ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                        })
                        ->select(DB::raw('count(reference_id) as totalconverted') )
                        ->first();
        $resultsalesfranchise = json_decode(json_encode($datasalesfranchise), 1);
        $data['franchiseconverted'] = $resultsalesfranchise['totalconverted'];
        
        
        $dataleadfranchise = DB::table("franchise_leads")
                      ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                      ->where('franchise_leads.bpo_id',$bpo_id)
                      ->where('franchise_leads.status', 'In Progress')
                      ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                      })
                      ->select(DB::raw('count(franchise_leads.reference_id) as totallead'))
                        ->first();
        $resultleadfranchise = json_decode(json_encode($dataleadfranchise), 1);
        $data['leadfranchise'] = $resultleadfranchise['totallead'];
        
        $datanotinterstedfranchise = DB::table("franchise_leads")
                              ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                              ->where('franchise_leads.bpo_id',$bpo_id)
                              ->where('franchise_leads.status', 'Not Interested')
                              ->where(function($query) use ($role, $report_manager) {
                                if ($role == 'Team Lead') {
                                    $query = $query->where('users.reporting_manager', '=', " $report_manager");
                                } else if ($role == 'Tele Caller') {
                                    $query = $query->where('users.id', '=', " $report_manager");
                                } elseif ($role == 'Assistant Manager') {
                                    $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                                }
                            })
                            ->select(DB::raw('count(franchise_leads.reference_id) as totallead'))
                            ->first();
        $resultnotinterstedfranchise = json_decode(json_encode($datanotinterstedfranchise), 1);
        $data['notinterstedfranchise'] = $resultnotinterstedfranchise['totallead'];
       

        $datareceivefranchise = DB::table("franchise_leads")
                         ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                         ->where('franchise_leads.bpo_id', $bpo_id)
                         ->where('franchise_leads.status', 'Received')
                         //->where('franchise_leads.customer_status', 'Request')
                         ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }   
                       })
                       ->select(DB::raw('count(franchise_leads.reference_id) as totallead'))
                       ->first();
        $resultreceivefranchise = json_decode(json_encode($datareceivefranchise), 1);
        $data['leadreceivefranchise'] = $resultreceivefranchise['totallead'];
        
        $datacallback = $this->callback_detail("franchise")
                              ->select(DB::raw('count(callback_alerts.id) as callbackdata'))
                              ->first();
        $resultcallback = json_decode(json_encode($datacallback), 1);
        $data['franchisecallback'] = $resultcallback['callbackdata'];
        $data['controller'] = "dashboard";
        $data['action'] = "";
        return view('dashboard',$data);
    }

    public function callback_detail($type) {
		    $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        return DB::table("callback_alerts")
                  ->Join("distributor_leads", "distributor_leads.reference_id",'=',"callback_alerts.customer_id")
                  ->Join("users", "distributor_leads.rel_manager_id",'=',"users.id")
                  ->where('distributor_leads.bpo_id',$bpo_id)
                  ->where('callback_alerts.user_type',$type)
                  ->where('distributor_leads.status','In Progress')
                  ->where(DB::raw('date(callbacktime)'),'<=',DB::raw("date(now())"))
                  ->where(function($query) use ($role, $report_manager) {
                        if ($role == 'Team Lead') {
                            $query = $query->where('users.reporting_manager', '=', " $report_manager");
                        } else if ($role == 'Tele Caller') {
                            $query = $query->where('users.id', '=', " $report_manager");
                        } elseif ($role == 'Assistant Manager') {
                            $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                        }
                   });
    }

    public function callBackData($type){
        $result = $this->callback_detail($type)
                      ->select('distributor_leads.name as leadname','reference_id','users.name as username','callbacktime','distributor_leads.mobile as leadmobile','distributor_leads.email as leademail')
                      ->paginate(50);
        $data['result'] = $result;
        $data['controller'] = "";
        $data['action'] = "";
        return view('call-back',$data);
    }

}
