<?php
namespace App\Http\Controllers;
use Mail;
use App\Interactions;
use App\User;
use App\DistributorLead;
use App\SmsSentDetail;					  
use App\CallbackAlerts;
use DateTime;
use Validator;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use App\VerifierInteraction;
use DB;
use Session;
use App\EmailTemplate;					  
use Bitly;
use App\States;	  

class LeadController extends Controller {

    public function index() {
        $data['states'] = getstate();
        $data['cities'] = array();
        $data['controller'] = "lead";
        $data['action'] = "leadadd";
        $data['leadsourcevalue'] = leadsource();
        $data['currentprofession'] = currentprofession();
        return view('lead/lead-create', $data);
    }

    public function citydetailskeyval(Request $request) {
        $stateid = $request['stateid'];
        return getcity($stateid);
    }

    public function leadCreate(Request $request) {
        $data = $this->addlead($request);
        if((Auth::user()->role=="Tele Caller") || (Auth::user()->role == "admin" && Auth::user()->status == "inactive")){
            return redirect()->route('lead-details', [$data->reference_id]);
        }
        return redirect()->route('unassigned-lead');
    }

    public function leadList(Request $request, $sep = null) {
        $arrsearch = array('option'=>$request['option'],'q'=>$request['q']);
        $role = Auth::user()->role;
        $user_id = Auth::user()->id;
        $dataunion = DB::table("distributor_leads")
                       ->join("users","id",'=','rel_manager_id')
                       ->leftjoin("interactions","interactions.interaction_id",'=','distributor_leads.last_interaction_id')
                       ->where("distributor_leads.status",'In progress')
                       ->where("distributor_leads.bpo_id", Auth::user()->bpo_id)
                       ->whereNotNull('rel_manager_id')
                       ->where(function($q) use ($role,$user_id){
                            if($role=='Team Lead'){
                                $q->where("reporting_manager",$user_id);
                            }elseif($role=='Tele Caller'){
                                $q->where("rel_manager_id",$user_id);
                            }
                        })
                        ->where(function ($query) use ($arrsearch) {
                              if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('distributor_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('distributor_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('distributor_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('distributor_leads.email', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Status')
                                    $query = $query->where('distributor_leads.status', 'LIKE', "%$search%");
                            }
                        })
                        ->select("distributor_leads.reference_id", "distributor_leads.name", "distributor_leads.city_name", "distributor_leads.mobile", "distributor_leads.state_name", "distributor_leads.email", "distributor_leads.created_at", DB::raw("Null as applicant_name"), DB::raw("'Lead' as type"), "distributor_leads.rel_manager_id", "distributor_leads.lead_source","distributor_leads.customer_status", "distributor_leads.status","users.name as username","interactions.call_substatus_id","interactions.call_status_id")
                        ->orderBy('reference_id','desc')
                        ->paginate(10);

        $data['result'] = $dataunion;
        $data['controller'] = 'lead';
        $data['action'] = 'assign-lead-list';
		return view('lead.lead-list', $data)->with($arrsearch);
    }
	public function tlverifyleadList(Request $request, $sep = null) {
        $arrsearch = array('option' => $request['option'], 'q' =>$request['q']);
        $role = Auth::user()->role;
        $user_id = Auth::user()->id;
        $dataunion = DB::table("distributor_leads")
                       ->join("users","id",'=','rel_manager_id')
                       ->leftjoin("interactions","interactions.interaction_id",'=','distributor_leads.last_interaction_id')
                       ->where("distributor_leads.status",'In Progress')
                       ->where("distributor_leads.bpo_id", Auth::user()->bpo_id)
                       ->whereNotNull('rel_manager_id')
                       ->where(function($q) use ($role,$user_id){
                            if($role=='Team Lead'){
                                $q->where("reporting_manager",$user_id);
                            }elseif($role=='Tele Caller'){
                                $q->where("rel_manager_id",$user_id);
                            }
                        })
                        ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('distributor_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('distributor_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('distributor_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('distributor_leads.email', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Status')
                                    $query = $query->where('distributor_leads.status', 'LIKE', "%$search%");
                            }
                        })
                        ->select("distributor_leads.reference_id", "distributor_leads.name", "distributor_leads.city_name", "distributor_leads.mobile", "distributor_leads.state_name", "distributor_leads.email", "distributor_leads.created", DB::raw("Null as applicant_name"), DB::raw("'Lead' as type"), "distributor_leads.rel_manager_id","distributor_leads.customer_id", "distributor_leads.lead_source", "distributor_leads.lead_source_info", "distributor_leads.customer_status", "distributor_leads.status", "distributor_leads.associative_no", "distributor_leads.form_no","users.name as username","interactions.call_substatus_id","interactions.call_status_id")
                        ->orderBy('reference_id','desc')
                        ->paginate(10);

        $data['result'] = $dataunion;
        $data['controller'] = 'tllead';
        $data['action'] = 'tlassign-lead-list';
        return view('lms.tllead', $data)->with($arrsearch);
    }

    public function leadDetails($id) {
        $result = DB::table("distributor_leads")
                ->where('bpo_id',Auth::user()->bpo_id)
                ->where("distributor_leads.reference_id", '=',$id)
                ->where('distributor_leads.status','In Progress')
                ->select("distributor_leads.appointment_date", "distributor_leads.appointment_time", "distributor_leads.verifier", "distributor_leads.reference_id", "distributor_leads.mobile", "distributor_leads.name", "distributor_leads.email", "distributor_leads.created_at","distributor_leads.status", "distributor_leads.reference_id", "distributor_leads.json_struct", "distributor_leads.converted_customer_id", "distributor_leads.customer_status", "distributor_leads.stage_id", "distributor_leads.stage_remarks","rel_manager_id")
                  ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
       
        $data['reference_data'] = json_decode(json_encode($result), 1);
        $data['callstatus'] = CallStatusSubStatus();
        $interaction = Interactions::where('reference_id', '=', $id)
                ->orderBy('created_at', 'desc')
                ->get();
        $data['interaction'] = json_decode(json_encode($interaction), 1);
        $user = User::where('bpo_id', Auth::user()->bpo_id)
                ->lists('name', 'id');
        $data['rms'] = json_decode(json_encode($user), 1);
		$emailtemplate = EmailTemplate::where('status','active')
                                      ->select('title','id')
                                      ->get();
        $data['emailtemplate'] = json_decode(json_encode($emailtemplate),1);
        $data['controller'] = 'lead';
        $data['action'] = 'assign-lead-list';
        return view('lead/pendinglead-details', $data);
    }

    public function postInteraction(Request $request) {
        $data = array();
        if ($request->isMethod('post')) {
            $callstatusname = callstatus();
            $callsubstatusname = callsubstatus();
            $rms = User::lists('name', 'id');
            $data = array();

            $dispcallback = $callbackdate = "";
            if ($request->nextdatetime != ""){
                $callbackdate = date("Y-m-d h:i:s", strtotime($request->nextdatetime));
                $dispcallback = date("M d, Y H:i",strtotime($request->nextdatetime));
            }
            $interaction = new Interactions;
            $interaction->user_type = $request->user_type;
            $interaction->reference_id = $request->reference_id;
            $interaction->bpo_id = Auth::user()->bpo_id;
            $interaction->rm_id = Auth::user()->id;
            $interaction->call_initiator = $request->callinitiator;
            $interaction->call_status_id = $request->callstatus;
            $interaction->call_substatus_id = $request->callsubstatus;
            $interaction->call_substatus_other = $request->callsubstatusother;
            $interaction->callback_datetime = empty($callbackdate) ? null : $callbackdate;
            $interaction->remarks = $request->interaction;
            $interaction->save();

            $flagdelete = DB::table('callback_alerts')
                            ->where('customer_id', '=', $request->reference_id)
                            ->whereIn('user_type', ['distributor'])
                            ->delete();
            $sessiondata = $request->session()->get('notification') - $flagdelete;
            $request->session()->put('notification', $sessiondata);

            if ($request->nextdatetime != "" && $request->callsubstatus != "66") {
                $refdata = DistributorLead::where([['reference_id', $request->reference_id], ['bpo_id', Auth::user()->bpo_id]])->get()->toArray();
                $objcallback = new CallbackAlerts();
                $objcallback->customer_id = $request->reference_id;
                $objcallback->customer_name = $refdata[0]['name'];
                ;
                $objcallback->customer_mobile = $refdata[0]['mobile'];
                ;
                $objcallback->callbacktime = date("Y-m-d h:i:s", strtotime($request->nextdatetime));
                $objcallback->relationship_manager = Auth::user()->id;
                $objcallback->call_status_id = $request->callstatus;
                $objcallback->call_sub_status_id = $request->callsubstatus;
                $objcallback->user_type = 'distributor';
                $objcallback->bpo_id = Auth::user()->bpo_id;
                $objcallback->save();
                $dataconversion = date("Y-m-d", strtotime($request->nextdatetime));
                if (strtotime($dataconversion) == strtotime(date('Y-m-d'))) {
                    $sessiondata = $request->session()->get('notification') + 1;
                    $request->session()->put('notification', $sessiondata);
                }
            }
			$intarray = ['last_interaction_id'=>$interaction->interaction_id];
            if($request->callsubstatus=="79"){
                $intarray = array_merge($intarray,['status'=>'Received']);    
                $data['refresh'] = 1;    
            }
            $update_lead = DistributorLead::where('reference_id', $request->reference_id)
                     ->update($intarray);
            $data['interaction'] = array('datetime' => date("M d, Y H:i", strtotime($interaction->created_at)),
                'Logged_By' => $rms[$interaction->rm_id],
                'Remarks' => $interaction->remarks,
                'Call_Initiator' => $interaction->call_initiator,
                'Call_Status' => $callstatusname[$request->callstatus],
                'Call_Sub_Status' => $callsubstatusname[$request->callsubstatus],
                'Other' => $interaction->call_substatus_other,
                'Call_Back' => empty($dispcallback) ? "" : $dispcallback,
            );
            $data['return'] = true;
        }
        return $data;
    }

    function fixAppointment(Request $request) {
        $appointment_date = $request['appointment_date'];
        if ($appointment_date != ""){
            $reference_id = $request['reference_id'];
            return DistributorLead::where('reference_id',$reference_id)
						->update(['stage_id'=>1,'stage_remarks'=>'','appointment_date'=>date("Y-m-d H:i:s", strtotime($appointment_date))]);
        }
        return 0;
    }

    public function addlead($request) {
        $informed = $request['informed'];
        $emi_size = $request['emi_size'];
        $leadcreate = new DistributorLead();
        $leadcreate->name = $request['name'];
        $leadcreate->mobile = $request['mobile'];
        $leadcreate->email = $request['email'];
        $leadcreate->gender = $request['gender'];
        $leadcreate->state_id = $request['state'];
        $leadcreate->city_id = $request['city'];
        $leadcreate->area_id = $request['area'];
        $leadcreate->state_name = $request['statename'];
        $leadcreate->city_name = $request['cityname'];
        $leadcreate->area_name = $request['areaname'];
        $leadcreate->status = 'In Progress';
        $leadcreate->address = $request['address'];
        $leadcreate->pincode = $request['pincode'];
        $leadcreate->landmark = $request['landmark'];
        $leadcreate->company_name = $request['company_name'];
        $leadcreate->preffered_locations = $request['preffered_locations'];
        $leadcreate->customer_status = 'Draft';
        $leadcreate->total_experience = $request['total_experience'];
        $leadcreate->lead_source = $request['leadsourcevalue'];
        $leadcreate->other_lead_source = $request['other_lead_source'];
        $leadcreate->current_profession = $request['current_profession'];
        $leadcreate->other_current_profession = $request['other_current_profession'];
        $leadcreate->industry_type = $request['industry_type'];
        $leadcreate->subindustry_type = $request['subindustry_type'];
        $leadcreate->source = 'RM';
        $leadcreate->user_id = Auth::user()->id;
        if (Auth::user()->role == "Tele Caller" || (Auth::user()->role == "admin" && Auth::user()->status == "inactive")) {
            $leadcreate->rel_manager_id = !empty($request['user_id']) ? $request['user_id'] : Auth::user()->id;
        }
        $leadcreate->bpo_id = !empty($request['bpo_id']) ? $request['bpo_id'] : Auth::user()->bpo_id;
        if(Session::has('user_id')){
            $leadcreate->crm_user_id = Session::get('user_id');
        }
        $leadcreate->save();
        return $leadcreate;
    }

    public function leadedit(Request $request) {
        $state = $request['state'];
        $city = $request['city'];
        $emi_size = $request['emi_size'];
        $leadcreate = DistributorLead::findOrFail($request['reference_id']);
        $leadcreate->name = $request['name'];
        $leadcreate->mobile = $request['mobile'];
        $leadcreate->email = $request['email'];
        $leadcreate->state_id = $state;
        $leadcreate->district_id = $city;
        $leadcreate->state_name = $request['statename'];
        $leadcreate->city_name = $request['cityname'];
        $leadcreate->informed_to_reference = 'No';
        $leadcreate->status = 'In Progress';
       // $leadcreate->customer_status = 'Draft';
        $leadcreate->lead_source = $request['leadsourcevalue'];
        $leadcreate->source = 'RM';
        if (Auth::user()->role == "Tele Caller") {
            $leadcreate->rel_manager_id = !empty($request['user_id']) ? $request['user_id'] : Auth::user()->id;
        }
        $leadcreate->emi_size = $emi_size;
        $leadcreate->verifier = $alldata['verifier'];
        $leadcreate->created = new DateTime();
        $leadcreate->modified = new DateTime();
        $leadcreate->bpo_id = !empty($request['bpo_id']) ? $request['bpo_id'] : Auth::user()->bpo_id;
        $leadcreate->save();
        return redirect()->route('lead-list');
    } 

    public function generatelink(Request $request){
        $id = $request['id'];
        $token = Session::get('api_key');
        $url = config('custom.selfurl').'register/?id='.base64_encode(http_build_query(['id'=>$id,'api_token'=>$token]));
        return $url;
    }

    public function checkemailmobile(Request $request){
        $email = $request['email'];
        $mobile = $request['mobile'];
        $leadid = $request['lead_id'];
        $data = DistributorLead::where('mobile',$mobile)
                    ->where(function($q) use ($leadid){
                        if(!empty($leadid)){
                            $q->where('reference_id','!=',$leadid);
                        }
                    })
                    //->orWhere('email',$email)
                    ->first();
        if(empty($data)){
           return 0;
        }
        return 1;
    }

    public function editlist(){
        $result = DB::table("leads")
                    ->where('bpo_id',Auth::user()->bpo_id)
                    ->where("leads.reference_id", '=',$id)
                    ->where('leads.status','In Progress')
                    ->select("leads.appointment_date", "leads.appointment_time", "leads.verifier", "leads.reference_id", "leads.mobile", "leads.name", "leads.email", "leads.created", "leads.informed_to_reference", "leads.emi_size", "leads.status", "leads.reference_id", "leads.json_struct", "leads.email", "leads.converted_customer_id", "leads.customer_status", "leads.associative_no", "leads.form_no", "leads.stage_id", "leads.stage_remarks","rel_manager_id")
                    ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
    }

    public function changerequestref($id){
        $result = DistributorLead::where('bpo_id',Auth::user()->bpo_id)
                      ->where("leads.reference_id",'=',$id)
                      ->where('leads.status','In Progress')
                      ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
        $data['reference_data'] = json_decode(json_encode($result),1);
        $state = getstate();
        $data['states'] = $state;
        $data['nomstates'] = $state;
        $data['controller'] = 'lead';
        $data['action'] = 'assign-lead-list';
        return view('lead.changerequest',$data);
    }

    public function getcitydistrictpincode(Request $request){
        $stateId = $request['stateid'];
        return citydistrictpincode($stateId);
    }

    public function getpincodelist(Request $request){
        $district = $request['district'];
        return pincode($district);
    }
    
    public function getdistrictlist(Request $request){
        $pincode = $request['pincode'];
        return district($pincode);
    }

    public function checkemail(Request $request){
        $email = $request['email'];
        return checkloginemail($email);
    }

    public function sendlinkpayment(Request $request){
        $id = $request['leadid'];
        $leaddata = DistributorLead::where('bpo_id',Auth::user()->bpo_id)
                        ->where("leads.reference_id",'=',$id)
                        ->where('leads.status','In Progress')
                        ->first();
        if(!empty($leaddata)){
            $token = Session::get('api_key');
          //  echo $url = Bitly::getUrl(config('custom.selfurl').'register/payment?id='.base64_encode(http_build_query(['id'=>$id,'api_token'=>$token])));
			   echo $url = config('custom.selfurl').'register/payment?id='.base64_encode(http_build_query(['id'=>$id,'api_token'=>$token]));
           // $email['messages'] = "Please Do the Payment with the following link $url";
            $email['messages'] = 'Thanks for showing Interest in your CGP plans. Please click following link for making a payment online <a href='.$url.'>pay</a>';
			$email['to_mail'] = $leaddata->email;
            $email['to_name'] = $leaddata->name;
            $email['from_email'] = "customerservice@cherishgold.com";
            $email['from_name'] = "Cherishgold";
            $email['subject'] = "Pay Now Link";
            $this->sendemail($email);
            return 1;
        }
        return 0;
    }


    public function sendsmslinkpayment(Request $request){
        $id = $request['leadid'];
        $leaddata = DistributorLead::where('bpo_id',Auth::user()->bpo_id)
                        ->where("leads.reference_id",'=',$id)
                        ->where('leads.status','In Progress')
                        ->first();
        if(!empty($leaddata)){
            $token = Session::get('api_key');
            $url = Bitly::getUrl(config('custom.selfurl').'register/payment?id='.base64_encode(http_build_query(['id'=>$id,'api_token'=>$token])));
            $message = 'Thanks for showing Interest in your CGP plans. Please click following link for making a payment online '.$url;
            $sendSmsDetail = new SmsSentDetail();
            $sendSmsDetail->user_id = Auth::user()->id;
            $sendSmsDetail->bpo_id = Auth::user()->bpo_id;
            $sendSmsDetail->lead_id = $leaddata->reference_id;
            $sendSmsDetail->mobile_no = $leaddata->mobile;
            $sendSmsDetail->content = $message;
            if($this->sendsms($leaddata->mobile,$message)) {
                $sendSmsDetail->save();
            }
            return 1;
        }
        return 0;
    }
    
    public function sendregisterationlink(Request $request){
        $id = $request['id'];
        $leaddata = DistributorLead::where('bpo_id',Auth::user()->bpo_id)
                        ->where("leads.reference_id",'=',$id)
                        ->where('leads.status','In Progress')
                        ->first();
        if(!empty($leaddata)){
           // $url = Bitly::getUrl($this->generatelink($request));
			    $url = $this->generatelink($request);
            $email['messages'] = 'Thanks for showing Interest in your CGP plans. Please click following link for making a payment online <a href='.$url.'>pay</a>';
            $email['to_mail'] = $leaddata->email;
            $email['to_name'] = $leaddata->name;
            $email['from_email'] = "customerservice@cherishgold.com";
            $email['from_name'] = "Cherishgold";
            $email['subject'] = "Pay Now Link";
            $this->sendemail($email);
            return 1;
        }
        return 0;
    }

    public function sendsmsregisterationlink(Request $request){
       $id = $request['id'];
        $leaddata = DistributorLead::where('bpo_id',Auth::user()->bpo_id)
                        ->where("leads.reference_id",'=',$id)
                        ->where('leads.status','In Progress')
                        ->first();
        if(!empty($request->url)){
            //$url = Bitly::getUrl($this->generatelink($request));
            $url = Bitly::getUrl($request->url);
            $message = 'Thanks for showing Interest in your CGP plans. Please click following link for making a payment online '.$url;
		
            $sendSmsDetail = new SmsSentDetail();
            $sendSmsDetail->user_id = Auth::user()->id;
            $sendSmsDetail->bpo_id = Auth::user()->bpo_id;
            $sendSmsDetail->lead_id = $leaddata->reference_id;
            $sendSmsDetail->mobile_no = $leaddata->mobile;
            $sendSmsDetail->content = $message;
            if($this->sendsms($leaddata->mobile,$message)) {
                $sendSmsDetail->save();
            }
            return 1;
        }
        return 0;
    }

    public function edit_lead_list(Request $request, $reference_id) {
        $bpo_id = Auth::user()->bpo_id;
        $leadsourcevalue = DB::table('distributor_leads')
                             ->where('reference_id', $reference_id)
                             ->where('status','In Progress')
                             ->where('bpo_id', $bpo_id)
                             ->select("*")
                             ->first();
        $leaddata = json_decode(json_encode($leadsourcevalue), 1);
        $data['states'] = getstate();
        $data['cities'] = array();
        $data['leadvalue'] = $leaddata;
        $data['leaddatalist'] = json_decode($data['leadvalue']['verifier']);
        $data['controller'] = 'lead';
        $data['action'] = 'edit_lead_list';
        $leadsourcevalue = DB::table('lead_source')
                             ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                             ->get();
        $data['leadsourcevalue'] = $leadsourcevalue;
        return view('lead/lead-editdata', $data);
    }

    
    
    public function tl_edit_verifier(Request $request, $reference_id) {
        $bpo_id = Auth::user()->bpo_id;
        $leadsourcevalue = DB::table('leads')
                             ->where('reference_id', $reference_id)
                             ->where('status','In Progress')
                             ->where('bpo_id', $bpo_id)
                             ->select("*")
                             ->first();
        $leaddata = json_decode(json_encode($leadsourcevalue), 1);
        
       $result = DB::table("leads")
                ->where('bpo_id',Auth::user()->bpo_id)
                ->where("leads.reference_id", '=',$reference_id)
                ->where('leads.status','In Progress')
                ->select("leads.appointment_date", "leads.appointment_time", "leads.verifier", "leads.reference_id", "leads.mobile", "leads.name", "leads.email", "leads.created", "leads.informed_to_reference", "leads.emi_size", "leads.status", "leads.reference_id", "leads.json_struct", "leads.converted_customer_id", "leads.customer_status", "leads.associative_no", "leads.form_no", "leads.stage_id", "leads.stage_remarks","rel_manager_id")
                  ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
        $data['reference_data'] = json_decode(json_encode($result), 1);
        $data['callstatus'] = CallStatusSubStatus();
        $interaction = Interactions::where('reference_id', '=', $reference_id)
                ->orderBy('created_at', 'desc')
                ->get();
        $data['interaction'] = json_decode(json_encode($interaction), 1);
        $user = User::where('bpo_id', Auth::user()->bpo_id)
                ->lists('name', 'id');
        $data['rms'] = json_decode(json_encode($user), 1);
        $emailtemplate = EmailTemplate::where('status','active')
                                      ->select('title','id')
                                      ->get();
        $data['emailtemplate'] = json_decode(json_encode($emailtemplate),1);
        
        
        
        
        $data['states'] = getstate();
        $data['cities'] = array();
        $data['leadvalue'] = $leaddata;
        $data['leaddatalist'] = json_decode($data['leadvalue']['verifier']);
        $data['controller'] = 'lead';
        $data['action'] = 'tl-edit-verifier';
        $leadsourcevalue = DB::table('lead_source')
                             ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                             ->get();
        $data['leadsourcevalue'] = $leadsourcevalue;
        return view('lms/tl-edit-verifier', $data);
    }


			 
    public function leadinterestedlist(Request $request) {
        $arrsearch=['q'=>$request['q'],'option'=>$request['option']];
		$result = DB::table("distributor_leads")
                    ->join("users", "id", '=', 'rel_manager_id')
                    ->where("distributor_leads.status", 'In progress')
                    ->where("distributor_leads.bpo_id", Auth::user()->bpo_id)
                    ->select("distributor_leads.name as leadname", "distributor_leads.mobile as leadmobile", "distributor_leads.email as leademail","distributor_leads.reference_id as leadid","users.name as username")
                    ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('distributor_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('distributor_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('distributor_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('distributor_leads.email', 'LIKE', "%$search%");
                            }
                        })
                    ->paginate(20);
        $data['controller'] = "notinterested";
        $data['action'] = 'notinterestedlist';
        $data['leadsourcevalue'] = $result;
        return view('lead.lead-notinterested-list', $data)->with($arrsearch);
    }
    public function leadinteresteddetails(Request $request) {
        $arrsearch=['q'=>$request['q'],'option'=>$request['option']];
        $result = DB::table("distributor_leads")
                    ->join("users", "id", '=', 'rel_manager_id')
                    ->where("distributor_leads.status", 'Not Interested')
                    ->where("distributor_leads.bpo_id", Auth::user()->bpo_id)
                    ->select("distributor_leads.name as leadname", "distributor_leads.mobile as leadmobile", "distributor_leads.email as leademail","distributor_leads.reference_id as leadid","users.name as username")
                    ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('distributor_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('distributor_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('distributor_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('distributor_leads.email', 'LIKE', "%$search%");
                            }
                        })
                    ->paginate(20);
        $data['controller'] = "notinterested";
        $data['action'] = 'notinterestedlist';
        $data['leadsourcevalue'] = $result;
        return view('lead.lead-notinteresteddetails', $data)->with($arrsearch);
    }
    public function leadrecevieddetails(Request $request) {
        $arrsearch=['q'=>$request['q'],'option'=>$request['option']];
        $result = DB::table("distributor_leads")
                    ->join("users", "id", '=', 'rel_manager_id')
                    ->where("distributor_leads.status", 'Received')
                    ->where("distributor_leads.bpo_id", Auth::user()->bpo_id)
                    ->select("distributor_leads.name as leadname", "distributor_leads.mobile as leadmobile", "distributor_leads.email as leademail","distributor_leads.reference_id as leadid","users.name as username")
                    ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('distributor_leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('distributor_leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('distributor_leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('distributor_leads.email', 'LIKE', "%$search%");
                            }
                        })
                    ->paginate(20);
        $data['controller'] = "notinterested";
        $data['action'] = 'notinterestedlist';
        $data['leadsourcevalue'] = $result;
        return view('lead.lead-receviedlist', $data)->with($arrsearch);
    }

    public function marknotinterested(Request $request){
        $id = $request['id'];
        return DistributorLead::where('reference_id',$id)
                    ->update(['status'=>'Not Interested']);

    }

    public function markrecevied(Request $request){
        $id = $request['id'];
        return DistributorLead::where('reference_id',$id)
                    ->update(['status'=>'Received']);

    }

    public function leadchangerm(Request $request) {
        $rmid = $request['rm_id'];
        if(!$request->ajax()){
            //if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
            $ref_id=$request->session()->get('ref_value');
            $lead_id = array_reduce($ref_id, 'array_merge', array());
            $this->changerm($lead_id,$rmid);
            return redirect()->back()->with(['messages'=>'RM Change Successfully!!']);
        }else{
            $lead_id = [$request['lead_id']];
            return $this->changerm($lead_id,$rmid);
        }
        

    }

    public function changerm($lead,$rm){
        return DistributorLead::whereIn('reference_id',$lead)
                   ->update(['rel_manager_id'=>$rm]);
    }

    public function leadchangermlist(Request $request) {
          $arrsearch = ['q'=>$request['q'],'option'=>$request['option']];
										  
        $role = Auth::user()->role;
        if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
        }
        $tl = User::where('bpo_id',Auth::user()->bpo_id)
                  ->where('status','active');
        if($role=='Assistant Manager'){
            $tl = $tl->where('reporting_manager',Auth::user()->id);
        }elseif($role=='Team Lead'){
            $tl = $tl->where('id',Auth::user()->id);
        }
        $tl = $tl->where('role','Team Lead')
                 ->pluck('id');
        $user = DB::table('users as a1')
                  ->join('users as a2','a1.reporting_manager','=','a2.id')
                  ->where('a1.role','Tele Caller')
                  ->where('a1.bpo_id',Auth::user()->bpo_id)
                  ->whereIn('a1.reporting_manager',$tl)
                  ->where('a1.status','active')
                  ->select('a1.name as tellcallername','a1.id as tellcallerid','a2.name as tlname')
                  ->get();
        $data['reporting'] = json_decode(json_encode($user),1);

        $result = DB::table("distributor_leads")
                    ->join("users", "id",'=','rel_manager_id')
                    ->where("distributor_leads.status", 'In progress')
                    ->where("distributor_leads.bpo_id",Auth::user()->bpo_id)
                    ->where(function ($query) use ($arrsearch) {
                        if (!empty($arrsearch['option'])) {
                            $search = $arrsearch['q'];
                            if ($arrsearch['option'] == 'ID')
                                $query = $query->where('distributor_leads.reference_id', '=', "$search");
                            else if ($arrsearch['option'] == 'Name')
                                $query = $query->where('distributor_leads.name', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Mobile')
                                $query = $query->where('distributor_leads.mobile', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Email')
                                $query = $query->where('distributor_leads.email', 'LIKE', "%$search%");
                        }
                    })
                    ->select("distributor_leads.name as leadname", "distributor_leads.mobile as leadmobile", "distributor_leads.email as leademail", "distributor_leads.rel_manager_id as rel_manager_id","distributor_leads.reference_id as leadid", "users.name as username")
                    ->paginate(5);
        $data['controller'] = "reportc";
        $data['action'] = 'leadchangerm';
        $data['leadsourcevalue'] = $result;
        return view('lead/lead-changerm',$data)->with($arrsearch);
    }

    public function getarealist(Request $request){
        $cityid = $request['cityid'];
        return getarea($cityid);
    }

}
