<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lead;

class RegisterController extends Controller
{
	public function register(){
		$data['controller']='';
		$data['action']='';
		return view('register.userregister',$data);
    }

    public function checkurl(Request $request){
        $parameter = $request->session()->get('urldata');
        parse_str(base64_decode($parameter),$arr);
        $message=[];
        if(!isset($arr['id'])){
            $message['error'] = "Invalid Link";
        }
        $id = $arr['id'];
        $bpo_id = $arr['bpo_id'];
        $data = Lead::where('reference_id',$id)
                    ->where('bpo_id',$bpo_id)
                    ->select('name','reference_id','mobile','email','status','customer_status','bpo_id')
                    ->first();
        if(empty($data)){
           	$message['error'] = "Invalid Id";   
        }
        if($data['customer_status']=="Active" && $data['status']=="Converted"){
            $message['error'] = "Already Customer";      
        }
        if(isset($message['error'])){
        	return $message;
        }else{
        	$message['success'] = json_decode(json_encode($data),1);
        }
        return $message;
	}

	//public function 
}
