<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\WebsiteLead;

class WebsiteLeadController extends Controller
{
	public function sendotp(Request $request){
		$mobile = $request->mobile;
		$num_str = sprintf("%06d",mt_rand(1,999999));
		$message = "Your Otp for Registration $num_str";
		$this->sendsms($mobile,$message);
		return ['success'=>true,'data'=>['opt'=>$num_str]];
	}

	public function registerWebsiteLead(Request $request){
		$data = WebsiteLead::where('email',$request['email'])
			           ->orWhere('mobile',$request['mobile'])
			           ->where('bpo_id', $request['bpo_id'])
				   ->first();
		if(empty($data)){
			$obj = new WebsiteLead();
			$obj->name = $request['name']; 
			$obj->email = $request['email'];
			$obj->location_id = $request['location_id'];
			$obj->location_name = $request['location_name'];
			$obj->mobile = $request['mobile'];
			$obj->bpo_id = $request['bpo_id'];
			$obj->source = $request['utm_source'];
			$obj->medium = $request['utm_medium'];
			$obj->campaign = $request['utm_campaign'];
			$obj->is_interested= $request['is_interested'];
			$obj->district_id= $request['district_id'];
			$obj->district_name= $request['district_name'];
			$obj->save();
			if($request['bpo_id'] == 0) {
				$this->sendMail($request);
			}
			return ['success'=>true,'data'=>1];	
		}
		return ['success'=>true,'data'=>0];	
	}

	public function getWebsiteLead(Request $request){
		$data = WebsiteLead::where('bpo_id',$request->bpo_id)
			           ->select('name','email','mobile','location_name','source','medium','campaign')
				   ->get();
		return ['success'=>true,'data'=>$data];			

	}
	
	public function sendMail(Request $request) {
		$content= 'Name : {name}<br />
			<br />
			Email : {email}<br />
			<br />
			Mobile : {mobile}<br />
			<br />
			Location : {location}<br />
			<br />
			<strong>Regards,</strong><br />
			Cherish Gold<br />
			<br />
			';
    
    		$message = str_replace(['{name}','{email}','{mobile}','{location}'],[$request['name'],$request['email'],$request['mobile'],$request['district_name']], $content);
	    	$email['messages'] = $message;
			//$email['to_mail'] = ['deepak@birlagold.com','vikrant@birlagold.com','surender@birlagold.com'];
	    	$email['to_mail'] = ['nikita@birlagold.com','sachit@birlagold.com','prasanna@kserasera.com','salman.khan@birlagold.com','arvindo@adznetworkmedia.com','faridi.nazmeen@cherishgold.com'];
	    	$email['to_name'] = 'Admins';
	    	$email['from_email'] = "customerservice@cherishgold.com";
	    	$email['from_name'] = "Cherishgold";
	   	$email['subject'] = "Campaign Lead";
	    	$this->sendemail($email);
	    	
	    	$content= '
				Thanks a lot '.$request['name'].'&nbsp; for your interest. We will get back to you soon!<br />
				<br />
				<br />
				<strong>Regards,<br />
				Support Team,</strong><br />
				Cherish Gold<br />
				<br />

			';
    
    		$message =  $content;
	    	$email['messages'] = $message;
	    	$email['to_mail'] = $request['email'];
	    	$email['to_name'] =$request['name'];
	    	$email['from_email'] = "customerservice@cherishgold.com";
	    	$email['from_name'] = "Cherishgold";
	   	$email['subject'] = "Thank You";
	    	$this->sendemail($email);
	    	return 1;
	}



}
