<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Excel;
use Auth;
use DB;
use Mail;
use App\Repository\MasterRepository;
use App\BpoDetail;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
	
	public function response($viewFileName, $data,$iswith=false,$iswithdata=array(),$flag=0)
    {	if(isset(getallheaders()['api_token'])){
			 return $data;
		}
		elseif($flag==1) { return $data; }
		else{
			if($data['status'] == 0) {
				request()->session()->flash('status',$data['msg'] );
			}
			if($iswith == true) {
				return view($viewFileName,compact('data'))->with($iswithdata);
			
			}
			return view($viewFileName,compact('data'));
			
        }
       
    }

	 public function createexcel($filename, $data, $header) {
        Excel::create($filename . date("d-m-Y"), function($excel) {
            $excel->sheet('Sheet 1', function($sheet) {
                
            });
        })->store('csv', storage_path('excel'));
        Excel::load('storage/excel/' . $filename . date("d-m-Y") . '.csv', function($doc) use ($data, $header, $filename) {
            $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $sheet = $doc->setActiveSheetIndex(0);
            for ($i = 0; $i < count($header); $i++) {
                $sheet->setCellValue($string[$i] . '1', $header[$i]);
            }
            $append = 1;
            for ($j = 0; $j < count($data); $j++) {
                $append = $append + 1;
                $i = 0;
                foreach ($data[$j] as $value) {
                    $sheet->setCellValue($string[$i] . $append, $value);
                    $i++;
                }
            }
            unlink(storage_path('excel/' . $filename . date("d-m-Y") . '.csv'));
        })->download('csv');
    }
    
    public function assitance_manager_to_tl() {        
        $bpo_id = Auth::user()->bpo_id;
        $user_id = Auth::user()->id;
        $q="select a3.id from (SELECT a2.id as report_id FROM `users` as a1 inner join users as a2 on a1.id=a2.reporting_manager where a1.role='Assistant Manager' and a1.bpo_id='$bpo_id' and a1.id='$user_id') as tbl inner join users as a3 on a3.reporting_manager=tbl.report_id and a3.bpo_id='$bpo_id'";
        $data = DB::select($q);
        
        return json_decode(json_encode($data),1);
        
    }

    public function sendemail($data){
        Mail::send(['html' => 'email.layout'],['content'=>$data['messages']],function($message) use($data){
			$data['from_email'] = "customerservice@cherishgold.in";
			$data['from_name'] = "Cherishgold";
            $message->to($data['to_mail'],$data['to_name']);
            $message->from($data['from_email'],$data['from_name']);
            $message->subject($data['subject']);
            if(array_key_exists('attachfile',$data)){
                foreach($data['attachfile'] as $file){
                    $message->attach($file);
                }   
            }
        });
    }
    public function setsessiondata($request,$key,$checkbox){
        $page = $request['currentpage'];
        $fianlvalue=$request->session()->get($key);
        if($fianlvalue!=null){
            if((array_key_exists($page,$fianlvalue))){
               unset($fianlvalue[$page]);
            }
        }
        if(count($request[$checkbox])>0){
            $fianlvalue[$page]=$request[$checkbox];
        }
        if(!empty($page)){
            $request->session()->flash($key,$fianlvalue); 
        }
        return true;
    }
    
    public function sendsms($mobile,$message){
        $sms_site=env('SMS_SITE');
        $sms_method=env('SMS_METHOD');
        $sms_api_key=env('SMS_API_KEY');
        $sms_sender_id=env('SMS_SENDER_ID');
	
        $sendsms = new SendSmsController($sms_site, $sms_method,$sms_api_key, $sms_sender_id);
        $sendsms->send_sms($mobile,$message,'','xml');
		return true;
    }
    public static function getStates(MasterRepository $obj) {
        return $obj->state();
    }

    public static function getCities(MasterRepository $obj) {
        return $obj->city(request()->state);
    }

    public static function getAreas(MasterRepository $obj) {
        return $obj->area(request()->city);
    }

    public static function getCallStatus(MasterRepository $obj) {
        return $obj->callStatus();
    }

    public static function getCallSubStatus(MasterRepository $obj) {
        return $obj->callSubStatus();
    }
    public function checkbpo(){
        $bpodata = BpoDetail::where('bpo_id',Auth::user()->bpo_id)
                            ->first(['api_key','name','upload','status']);
        if($bpodata->status=='inactive'){
            return ['status'=>false];
        }
        return ['status'=>true,'data'=>$bpodata];
    }

}
