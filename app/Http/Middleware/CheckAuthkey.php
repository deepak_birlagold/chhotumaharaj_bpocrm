<?php

namespace App\Http\Middleware;

use Closure;
use App\BpoDetail;
use App\User;
use Session;

class CheckAuthkey
{	
	 public function handle($request, Closure $next){
         //header('Access-Control-Allow-Origin: *');  
        $header = getallheaders();
        $usertoken = $header['User-Token'];
        if(!empty($usertoken)){
        	$user=User::where('api_token',$usertoken)
        	    	  ->first();

        	if(empty($user)){
        		return response(['success'=>false ,'error'=>'Invalid Login Credential','data'=>[]],401);
        	}else{
        		$bpo = $user->bpoDetail()->first();
        		$arrvalue = explode("__",$bpo->api_key);
        		Session::put('api_key',$arrvalue[0]);
        	}
        	$request->merge(['user'=>$user]);
        }else{
			$token = $header['Api-Token'];
	        if($token===sha1('vikrantdeepaktoken')){
	            $dbdata = (new \stdClass());
	            $dbdata->bpo_id = 0;
	        }else{
		        $arrvalue = explode("__",$token);
		        $dbdata=BpoDetail::where('api_key',$token)
		                         ->first();
		        if((empty($dbdata))){
					$array=array('success'=>false,'message'=>'Invalid Token');
		            return response($array,422);
		        }
		        Session::put('api_key',$arrvalue[0]);
			}
	        $request->merge(['bpo_id'=>$dbdata->bpo_id]);
	    }
	   return $next($request);
        
    }
}
