<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class FranchiseLeadInteraction extends Model{
    protected $primaryKey = 'id';
    public $table="franchise_lead_interactions";

    public function callsubstatus(){
        return $this->hasOne('App\CallSubStatus',"sub_status_id","call_substatus_id");
    }
}
     
     
?>