<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    
    public $timestamps = false;
	
	protected $fillable = ['state_id','city_id','area_name'];
	
	public function states() {
		return $this->belongsTo('App\State', 'state_id');
	}
	public function cities() {
		return $this->belongsTo('App\City', 'cities_id');
	}
	public function domname() {
		return $this->hasMany('App\Dom', 'area_id','id');
	}
}
