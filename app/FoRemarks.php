<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoRemarks extends Model
{
	protected $connection = 'mysql';
    protected $primaryKey = 'id';
    public $table="fo_remarks";
}
