<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifierCallSubStatus extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    public $table="verifier_call_sub_status";
	 
	public function verifierCallSubStatus()
    {
     	return $this->belongsTo("App\VerifierCallStatus","id","id");
    }
}
