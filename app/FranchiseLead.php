<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class FranchiseLead extends Authenticatable {

    public $table = "franchise_leads";
    protected $fillable = ['id','distributor_id','rel_manager_id','bpo_id','last_interaction_id','bpo_lead_id','user_id','name','email','mobile','alternate_no','state_id','city_id','area_id','state_name','city_name','area_name','address','pincode','source','other_source','existing_property','details_of_property','ready_finance_for_project','how_much_finance','looking_for_finance','desired_finance_loan_amount','reason_for_franchise','status','crm_user_id','customer_status','preffered_locations'];

    public $timestamps = true;
    public $primaryKey = "reference_id";

}
