<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\FranchiseRepository;
use App\FranchiseLead;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('franchise', function ($app) {
            return new FranchiseRepository(request(),new FranchiseLead);
        });
    }
}
