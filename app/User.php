<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	public $table = "users";
    protected $fillable = [
        'name', 'email', 'password','role','bpo_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /* Vikrant Code */
    public function bpoDetail() {
        return $this->belongsTo('App\BpoDetail', 'bpo_id', 'bpo_id');
    }

    public function granttoken() {
        $this->api_token = str_random(60);
        $this->save();
        return $this;
    }
    /* Vikrant Code end */
}
