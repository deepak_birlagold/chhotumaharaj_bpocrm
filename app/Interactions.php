<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Interactions extends Authenticatable
{
  
	public $table = "interactions";        
    public $primaryKey='interaction_id';

}
