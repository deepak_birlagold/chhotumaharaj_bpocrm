<?php
namespace App\Repository;
use Session;

abstract class BaseRepository{

	protected $model,$req;

	public function __construct($request){
		$this->req = $request;
	}
	
	public function getData(){
		return $this->model->get();
	}

	public function curlApiRequest($method = 'GET', $url, $body = array()) {
		$curl = curl_init();
		$token = Session::get('token');
		$header = ['Accept: application/json',"Authorization: Bearer $token"];
		curl_setopt($curl, CURLOPT_URL, config('custom.api_url').$url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST,$method);
		if($method == 'POST') {
			curl_setopt($curl,CURLOPT_POSTFIELDS,$body);
		}
		curl_setopt($curl, CURLOPT_HTTPHEADER,$header);
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if($err) {
		    return ['success'=>false,'error'=>"Something Went Wrong",'data'=>""];
		}else{
		    return ['success'=>true,'error'=>"",'data'=>$response];
		}
	}
}
?>