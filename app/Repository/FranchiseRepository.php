<?php
namespace App\Repository;
use Cache;
use App\User;
use App\BpoDetail;
use Illuminate\Http\Request;
use Hash;
use Session;
use App\FranchiseLead;
use DB;
use Auth;

class FranchiseRepository extends BaseRepository{

   	protected $model;

	public function __construct($req,$model){
		parent::__construct($req);	
		$this->model = $model; 
	}

	public function savefranchise(){
		$arr = [
			'user_id'=>$this->req->user->id,
			'bpo_id'=>$this->req->user->bpo_id,
			'name'=> $this->req->name,
			'email'=> $this->req->email,
			'mobile'=>$this->req->mobile,
			'alternate_no'=>$this->req->alternate_no,
			'state_id'=>$this->req->state_id,
			'city_id'=>$this->req->city_id,
			'state_name'=>$this->req->state_name,
			'city_name'=>$this->req->city_name,
			'area_name' =>$this->req->area_name,
			'address'=>$this->req->address,
			'pincode'=>$this->req->pincode,
			'source'=>$this->req->source,	
			'other_source'=>$this->req->other_source,	
			'existing_property'=>$this->req->existing_property,	
			'details_of_property'=>$this->req->details_of_property,	
			'ready_finance_for_project'=>$this->req->ready_finance_for_project,	
			'how_much_finance'=>$this->req->how_much_finance,	
			'looking_for_finance'=>$this->req->looking_for_finance,	
			'desired_finance_loan_amount'=>$this->req->desired_finance_loan_amount,	
			'reason_for_franchise'=>$this->req->reason_for_franchise,	
			'status'=>'In Progress',
      'customer_status'=>'Draft'	
		];
		$checkmobile = $this->model->where('mobile',$this->req->mobile)
									->first();
		if(empty($checkmobile)){
			return $this->model->create($arr);
		}else{
			return ['success'=>false ,'error'=>'Mobile Number already Exsists','data'=>[]];
		}			
		
	}

	public function franchsieList(){
        $dataunion = DB::table("franchise_leads")
                       ->join("users","id",'=','rel_manager_id')
                       ->leftjoin("interactions","interactions.interaction_id",'=','franchise_leads.last_interaction_id')
                       ->where("franchise_leads.status",'In Progress')
                       ->where("franchise_leads.bpo_id",$this->req->user->bpo_id)
                       ->select("franchise_leads.reference_id", "franchise_leads.name", "franchise_leads.city_name", "franchise_leads.mobile", "franchise_leads.state_name", "franchise_leads.email", "franchise_leads.created_at","users.name as username")
                        ->orderBy('reference_id','desc')
                        ->paginate(13);
        return ['success'=>true ,'error'=>'','result'=>$dataunion];
    }

    public function assignrm(){
    	$user = User::where('role','Tele Caller')
    				->where('bpo_id',$this->req->user->bpo_id)
                    ->where('status','active')
                    ->select('name as tellcallername','id as tellcallerid')
                    ->get();
        return ['success'=>true ,'error'=>'','result'=>$user];
    }

    public function assignrmlist(){
        $result = $this->model->where(function($q){
                                    $q->whereNull('rel_manager_id');
                                    $q->Orwhere('rel_manager_id',0);
                                })
                                ->where('bpo_id',$this->req->user->bpo_id)
                                ->where('status','In Progress')
                                ->orderBy('rel_manager_id','desc')
                                ->paginate(13);
        return ['success'=>true ,'error'=>'','result'=>$result];
    }

    public function changerm(){
    	  $ref_id=explode(",",$this->req->ref_value);
        $rm_id = $this->req->relationship_manager;
       	$result = $this->model->whereIn('reference_i',$ref_id)
                     ->update(['rel_manager_id'=>$rm_id]);
     	return ['success'=>true ,'error'=>'','result'=>$result];   
    }

    public function dashboard(){
		
		$received = 'received';
        $bpo_id = $this->req->user->bpo_id;
        $report_manager = $this->req->user->id;
        $role = $this->req->user->role;
        
        $datasales = DB::table("distributor_leads")
                        ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                        ->where('distributor_leads.bpo_id', $bpo_id)
                        ->where('distributor_leads.status', 'Converted')
                        ->where('distributor_leads.customer_status','Verified')
                        ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                        })
                        ->select(DB::raw('count(reference_id) as totalconverted') )
                        ->first();
        $resultsales = json_decode(json_encode($datasales), 1);
        $data['converted'] = $resultsales['totalconverted'];
        
        
        $datalead = DB::table("distributor_leads")
                      ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                      ->where('distributor_leads.bpo_id',$bpo_id)
                      ->where('distributor_leads.status', 'In Progress')
                      ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                      })
                      ->select(DB::raw('count(distributor_leads.reference_id) as totallead'))
                        ->first();
        $resultlead = json_decode(json_encode($datalead), 1);
        $data['lead'] = $resultlead['totallead'];
        
        $datanotintersted = DB::table("distributor_leads")
                              ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                              ->where('distributor_leads.bpo_id',$bpo_id)
                              ->where('distributor_leads.status', 'Not Interested')
                              ->where(function($query) use ($role, $report_manager) {
                                if ($role == 'Team Lead') {
                                    $query = $query->where('users.reporting_manager', '=', " $report_manager");
                                } else if ($role == 'Tele Caller') {
                                    $query = $query->where('users.id', '=', " $report_manager");
                                } elseif ($role == 'Assistant Manager') {
                                    $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                                }
                            })
                            ->select(DB::raw('count(distributor_leads.reference_id) as totallead'))
                            ->first();
        $resultnotintersted = json_decode(json_encode($datanotintersted), 1);
        $data['notintersted'] = $resultnotintersted['totallead'];
       

        $datareceive = DB::table("distributor_leads")
                         ->leftJoin("users", "distributor_leads.rel_manager_id", '=', "users.id")
                         ->where('distributor_leads.bpo_id', $bpo_id)
                         ->where('distributor_leads.status', 'Received')
                         //->where('distributor_leads.customer_status', 'Request')
                         ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }   
                       })
                       ->select(DB::raw('count(distributor_leads.reference_id) as totallead'))
                       ->first();
        $resultreceive = json_decode(json_encode($datareceive), 1);
        $data['leadreceive'] = $resultreceive['totallead'];


        $datacallback = $this->callback_detail("distributor")
                              ->select(DB::raw('count(callback_alerts.id) as callbackdata'))
                              ->first();
        $resultcallback = json_decode(json_encode($datacallback), 1);
        $data['distributorcallback'] = $resultcallback['callbackdata'];


        $datasalesfranchise = DB::table("franchise_leads")
                        ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                        ->where('franchise_leads.bpo_id', $bpo_id)
                        ->where('franchise_leads.status', 'Converted')
                        ->where('franchise_leads.customer_status','Verified')
                        ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                        })
                        ->select(DB::raw('count(reference_id) as totalconverted') )
                        ->first();
        $resultsalesfranchise = json_decode(json_encode($datasalesfranchise), 1);
        $data['franchiseconverted'] = $resultsalesfranchise['totalconverted'];
        
        
        $dataleadfranchise = DB::table("franchise_leads")
                      ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                      ->where('franchise_leads.bpo_id',$bpo_id)
                      ->where('franchise_leads.status', 'In Progress')
                      ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                      })
                      ->select(DB::raw('count(franchise_leads.reference_id) as totallead'))
                        ->first();
        $resultleadfranchise = json_decode(json_encode($dataleadfranchise), 1);
        $data['leadfranchise'] = $resultleadfranchise['totallead'];
        
        $datanotinterstedfranchise = DB::table("franchise_leads")
                              ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                              ->where('franchise_leads.bpo_id',$bpo_id)
                              ->where('franchise_leads.status', 'Not Interested')
                              ->where(function($query) use ($role, $report_manager) {
                                if ($role == 'Team Lead') {
                                    $query = $query->where('users.reporting_manager', '=', " $report_manager");
                                } else if ($role == 'Tele Caller') {
                                    $query = $query->where('users.id', '=', " $report_manager");
                                } elseif ($role == 'Assistant Manager') {
                                    $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                                }
                            })
                            ->select(DB::raw('count(franchise_leads.reference_id) as totallead'))
                            ->first();
        $resultnotinterstedfranchise = json_decode(json_encode($datanotinterstedfranchise), 1);
        $data['notinterstedfranchise'] = $resultnotinterstedfranchise['totallead'];
       

        $datareceivefranchise = DB::table("franchise_leads")
                         ->leftJoin("users", "franchise_leads.rel_manager_id", '=', "users.id")
                         ->where('franchise_leads.bpo_id', $bpo_id)
                         ->where('franchise_leads.status', 'Received')
                         //->where('franchise_leads.customer_status', 'Request')
                         ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }   
                       })
                       ->select(DB::raw('count(franchise_leads.reference_id) as totallead'))
                       ->first();
        $resultreceivefranchise = json_decode(json_encode($datareceivefranchise), 1);
        $data['leadreceivefranchise'] = $resultreceivefranchise['totallead'];
        
        $datacallback = $this->callback_detail("franchise")
                              ->select(DB::raw('count(callback_alerts.id) as callbackdata'))
                              ->first();
        $resultcallback = json_decode(json_encode($datacallback), 1);
        $data['franchisecallback'] = $resultcallback['callbackdata'];
        return ['success'=>true ,'error'=>'','result'=>$data];
    }

    public function assitance_manager_to_tl() {        
        $bpo_id = $this->req->user->bpo_id;
        $user_id = $this->req->user->id;
        $q="select a3.id from (SELECT a2.id as report_id FROM `users` as a1 inner join users as a2 on a1.id=a2.reporting_manager where a1.role='Assistant Manager' and a1.bpo_id='$bpo_id' and a1.id='$user_id') as tbl inner join users as a3 on a3.reporting_manager=tbl.report_id and a3.bpo_id='$bpo_id'";
        $data = DB::select($q);
        return json_decode(json_encode($data),1);
        
    }
    public function callback_detail($type) {
		$bpo_id = $this->req->user->bpo_id;
        $report_manager = $this->req->user->id;
        $role = $this->req->user->role;
        return DB::table("callback_alerts")
                  ->Join("distributor_leads", "distributor_leads.reference_id",'=',"callback_alerts.customer_id")
                  ->Join("users", "distributor_leads.rel_manager_id",'=',"users.id")
                  ->where('distributor_leads.bpo_id',$bpo_id)
                  ->where('callback_alerts.user_type',$type)
                  ->where('distributor_leads.status','In Progress')
                  ->where(DB::raw('date(callbacktime)'),'<=',DB::raw("date(now())"))
                  ->where(function($query) use ($role, $report_manager) {
                        if ($role == 'Team Lead') {
                            $query = $query->where('users.reporting_manager', '=', " $report_manager");
                        } else if ($role == 'Tele Caller') {
                            $query = $query->where('users.id', '=', " $report_manager");
                        } elseif ($role == 'Assistant Manager') {
                            $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                        }
                   });
    }
}