<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteLead extends Model
{
    public $table = "website_lead";        
    public $primaryKey='id';
}
