<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifierInteraction extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    public $table="verifier_interaction";


    public function interactionCallSubStatus(){
        return $this->hasOne('App\VerifierCallSubStatus',"verifier_call_status_id","verifier_call_sub_status_id");
    }
}
