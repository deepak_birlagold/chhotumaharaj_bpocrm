<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class CallSubStatus extends Model{
     
     protected $connection = 'mysql';
     protected $primaryKey = 'sub_status_id';
     public $table="call_sub_status";
	 
	 public function subStatus()
     {
     	return $this->belongsTo("App\CallStatus","status_id","status_id");
     }
}
     
     
?>