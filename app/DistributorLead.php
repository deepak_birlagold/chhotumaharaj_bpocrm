<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class DistributorLead extends Authenticatable {

    public $table = "distributor_leads";
    protected $fillable = [
        'reference_id', 'bpo_id',
    ];
    public $timestamps = true;
	public $primaryKey = "reference_id";

}
