<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class BpoDetail extends Authenticatable
{
  
	public $table = "bpo_details";
	protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'bpo_id', 'api_key','self_rm'
    ];
	public $timestamps = true;

	/* Vikrant Code */
	public function user() {
		return $this->hasMany('App\User', 'bpo_id', 'bpo_id');
	}
	/* Vikrant Code end */
}
