var FormValidation = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    number: {
                        required: true,
                        number: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    category: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success1.show();
                    error1.hide();
                }
            });

    }

    var handleValidation2 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form2 = $('#form_sample_2');
            var error2 = $('.alert-danger', form2);
            var success2 = $('.alert-success', form2);

            form2.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    number: {
                        required: true,
                        number: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success2.hide();
                    error2.show();
                    App.scrollTo(error2, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (form) {
                    success2.show();
                    error2.hide();
                }
            });


    }
	var editVerifier = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form03 = $('#editVerifier');
            var error03 = $('.alert-danger', form03);
            var success03 = $('.alert-success', form03);

            form03.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
                   name: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    landmark: {
                        required: true
                    },
                    pincode: {
                        minlength: 6,
                        maxlength:6,
                        number:true,
                    },
                    mobile: {
                        minlength: 10,
                        maxlength: 10,
                        number: true,
                        required: true
                    },
                    email: {
                        email: true
                    },
                    emi_size: {
                        number: true
                    },
                    state:{
                        required: true, 
                    },
                    city:{
                        required: true, 
                    },
                    age:{
                        number: true,
                    },
                    call_sub_status: {
                        required: true,
                    },
                    call_status: {
                        required: true,
                    },
                    appointment_time: {
                        required: true,
                    },
                    appointment_date: {
                        required: true,
                    },
                    verification_interaction: {
                        required: true,
                    },
                    emi_size:{
                        required:true,
                        number: true,
                        multiple1000:true,
                        min:1000
                    },
                    probability:{
                        required:true,
                        number: true,
                        max:100
                    },
                    ticket:{
                        required:true,
                        number: true,
                        multiple1000:true,
                        min:3000,
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success03.hide();
                    error03.show();
                    App.scrollTo(error03, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });

    }


    var spokupdate = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form03 = $('#spokupdate');
            var error03 = $('.alert-danger', form03);
            var success03 = $('.alert-success', form03);

            form03.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
                    call_sub_status: {
                        required: true,
                    },
                    call_status: {
                        required: true,
                    },
                    appointment_time: {
                        required: true,
                    },
                    appointment_date: {
                        required: true,
                    },
                    verification_interaction: {
                        required: true,
                    },
                    sel_fo:{
                        required: true,  
                    },
                    hour:{
                        required: true,    
                    },
                    minute:{
                        required: true,  
                    }

                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success03.hide();
                    error03.show();
                    App.scrollTo(error03, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });

    }


    var userCreate = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form3 = $('#userCreate');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            //form3.on('submit', function() {
             //   for(var instanceName in CKEDITOR.instances) {
              //      CKEDITOR.instances[instanceName].updateElement();
               // }
            //})

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        minlength: 6,
                        required: true
                    },
					password_confirmation: {
						minlength: 6,
						equalTo: "#password",
                        required: true
					},
                    emp_type: {
                        required: true
                    },
                    personal_email: {
                        required: true,
                        email: true
                    },
                    mobile: {
                        minlength: 10,
                        maxlength: 10,
                        required: true,
						number: true
                    },
                    address: {
                        required: true
                    },
                    "documents[]": {
                        required: true,
                        minlength: 2
                    },
                    dob: {
                        required: true
                    },
                    doj: {
                        required: true
                    },
                    role: {
                        required: true
                    },
					"language[]": {
						required: true,
                        minlength: 2,
						maxlength: 4
					},
                    "rights[]": {
                        required: true,
                        minlength: 1
                    },
                    state:{
                        required: true,   
                    },
                    city:{
                        required: true,  
                    },
                    bpo:{
                        required: true,  
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    emp_type: {
                        required: "Please select a Employment type"
                    },
                    role: {
                        required: "Please select a Employment Role"
                    },
                    rights: {
                        required: "Please select Rights"
                    },
                    documents: {
                        required: "Please select  at least 2 types of document",
                        minlength: jQuery.format("Please select  at least {0} types of document")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
    }
    var userEdit = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation
            var form3 = $('#userEdit');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);
		   //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form3.on('submit', function() {
               for(var instanceName in CKEDITOR.instances) {
                   CKEDITOR.instances[instanceName].updateElement();
               }
            })

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        minlength: 6
                    },
                    password_confirmation: {
                        minlength: 6,
                        equalTo: "#password"
                    },
                    emp_type: {
                        required: true
                    },
                    personal_email: {
                        required: true,
                        email: true
                    },
                    mobile: {
                        minlength: 10,
                        maxlength: 10,
                        required: true,
                        number: true
                    },
                    address: {
                        required: true
                    },
                    "documents[]": {
                        required: true,
                        minlength: 2
                    },
                    dob: {
                        required: true
                    },
                    doj: {
                        required: true
                    },
                    role: {
                        required: true
                    },
                    "language[]": {
                        required: true,
                        minlength: 2,
                        maxlength: 4
                    },
                    "rights[]": {
                        required: true,
                        minlength: 1
                    },
                    state:{
                        required: true,   
                    },
                    city:{
                        required: true,  
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    emp_type: {
                        required: "Please select a Employment type"
                    },
                    role: {
                        required: "Please select a Employment Role"
                    },
                    rights: {
                        required: "Please select Rights"
                    },
                    documents: {
                        required: "Please select  at least 2 types of document",
                        minlength: jQuery.format("Please select  at least {0} types of document")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
    }
	

	
	var customerRegistration = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form3 = $('#lead-details');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);
            
            $.validator.addMethod("multiple1000",function(value, element) {
                return value % 1000 == 0
            },"Amount Shold be multiple of 1000");

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    contact_email: {
						required: true,
                        email: true
                    },
					applicant_name: {
						required: true
					},
					contact_mobile: {
                        number: true,
						required: true,
						minlength: 10
					},
                    applicant_dob:{
                        required: true
                    },
                    applicant_gender:{
                        required: true  
                    },
                    mailing_address:{
                        required: true
                    },
                    mailing_landmark:{
                        required: true  
                    },
                    mailing_state:{
                        required: true  
                    },
                    mailing_city:{
                        required: true   
                    },
                    mailing_district:{
                        required: true  
                    },
                    mailing_pincode:{
                        required: true  
                    },
                    amount:{
                        required:true,
                        multiple1000:true
                    },
                    fulfilment:{
                      required:true  
                    },
                    lead_source:{
                      required:true  
                    },
                    lead_source_other:{
                        required:function(element){
                            return $('#lead_source_oth').is(':checked')
                        }
                    }
                },
                
               

                messages: { // custom messages for radio buttons and checkboxes
                   
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                   
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    //error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //form.submit();
                    validatedata();
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
    }

	var customerInteraction = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation  pendingcharge

            var form3 = $('#customer-interaction');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            // form3.on('submit', function() {
                // for(var instanceName in CKEDITOR.instances) {
                    // CKEDITOR.instances[instanceName].updateElement();
                // }
            // })

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    interaction: {
                        required: true
                    },
                    callsubstatus: {
                        required: true
                    },
					call_sub_status_other: {
                        required: true
                    },
					callstatus: {
						required: true
					},
                    callinitiator: {
                        required: true
                    },
                    nextdatetime:{
                        checkdatetime:true,
                    }
                },
               
                
                messages: { // custom messages for radio buttons and checkboxes
                    interaction: {
                        required: "Please enter a brief summary of the call"
                    },
                    callinitiator: {
                        required: "Please select one Call Initiator Option"
                    },
                    callstatus: {
                        required: "Please select one call Status"
                    },
					callsubstatus: {
						required: "Please select one call Sub Status"
					}
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //form.submit();
					postInteraction();
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
    }
	
	var validateCall = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form11 = $('.addcallinitates');
            var error11 = $('.alert-danger', form11);
            var success11 = $('.alert-success', form11);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form11.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    order: {
                        required: true
                    },
                    code:{
                      required: true  
                    }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success11.hide();
                    error11.show();
                    App.scrollTo(error11, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }
    var emailsmsvalidation = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form23 = $('#emailsms');
            var error23 = $('.alert-danger', form23);
            var success23 = $('.alert-success', form23);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form23.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    message: {
                        minlength: 2,
                        required: true
                    }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success23.hide();
                    error23.show();
                    App.scrollTo(error23, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }

     
    var validateLanguage = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form111 = $('#addLanguages');
            var error111 = $('.alert-danger', form111);
            var success111 = $('.alert-success', form111);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form111.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    language_name: {
                        minlength: 2,
                        required: true
                    },
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success111.hide();
                    error111.show();
                    App.scrollTo(error111, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }
	
	
        var validatecollectioncentre = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form100 = $('#validatecollectioncentre');
            var error100 = $('.alert-danger', form100);
            var success100 = $('.alert-success', form100);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form100.on('submit', function() {
                for(var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form100.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                        company_name:{
                        required: true
                       },
                       email:{
                        required: true
                       },
                       address:{
                        required: true
                       },
                       state:{
                        required: true
                       },
                       city:{
                        required: true
                       },
                       contact:{
                        minlength: 10,
                        maxlength: 10,
                        required:true
                       },
                       pincode:{
                        required: true,
                        number: true,
                        minlength: 6,
                        maxlength: 6
                       },
                       contact_person:{
                        required: true
                       },
                       bank:{
                        required: true
                       },
                       account_no:{
                        required: true
                       },
                       branch_address:{
                        required: true
                       },
                       ifsc_code:{
                        required: true
                       }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success100.hide();
                    error100.show();
                    App.scrollTo(error100, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }
    var validatecollectionsubcentre = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form101 = $('#validatecollectionsubcentre');
            var error101 = $('.alert-danger', form101);
            var success101 = $('.alert-success', form101);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form101.on('submit', function() {
                for(var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form101.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                        company_name:{
                        required: true
                       },
                       branch_name:{
                        required: true
                       },
                       email:{
                        required: true
                       },
                       address:{
                        required: true
                       },
                       state:{
                        required: true
                       },
                       city:{
                        required: true
                       },
                       pincode:{
                        required: true,
                        number: true,
                        minlength: 6,
                        maxlength: 6
                       },
                       contact:{
                        required: true
                       },
                       contact_person:{
                        required: true
                       },
                       limit:{
                        required: true
                       },
                       priority:{
                        required: true
                       },
                       services:{
                        required: true
                       }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success101.hide();
                    error101.show();
                    App.scrollTo(error101, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }
    var validatepincode = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form102 = $('#validatepincode');
            var error102 = $('.alert-danger', form102);
            var success102 = $('.alert-success', form102);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form102.on('submit', function() {
                for(var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form102.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                        company_name:{
                        required: true
                       },
                       branch_name:{
                        required: true
                       },
                       pincode:{
                        required: true
                       }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success102.hide();
                    error102.show();
                    App.scrollTo(error102, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }

     var pincodecsv = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form103 = $('#pincodecsv');
            var error103 = $('.alert-danger', form103);
            var success103 = $('.alert-success', form103);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form103.on('submit', function() {
                $('#loading').show();
                for(var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form103.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                        import_file:{
                        required: true
                       }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success103.hide();
                    error103.show();
                    App.scrollTo(error103, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }

    var validateemailtemplate = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form104 = $('#validateemailtemplate');
            var error104 = $('.alert-danger', form104);
            var success104 = $('.alert-success', form104);

            form104.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   title: {
                        minlength: 2,
                        required: true
                    },
                    from_name: {
                        minlength: 2,
                        required: true
                    },
                    from_email: {
                        minlength: 2,
                        required: true
                    },
                    subject: {
                        minlength: 2,
                        required: true
                    },
                    content: {
                        minlength: 2,
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success104.hide();
                    error104.show();
                    App.scrollTo(error104, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                   // success24.show();
                   // error24.hide();
                }
            });

    }

     var validatesmstemplate = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form105 = $('#validatesmstemplate');
            var error105 = $('.alert-danger', form105);
            var success105 = $('.alert-success', form105);

            form105.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   title: {
                        required: true
                    },
                    status: {
                        required: true
                    },

                    content: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success105.hide();
                    error105.show();
                    App.scrollTo(error105, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                                   }
            });

    }

	
    var addreference = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form105 = $('#addnewreference');
            var error105 = $('.alert-danger', form105);
            var success105 = $('.alert-success', form105);

            form105.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   name: {
                        required: true
                    },
                    mobile: {
                        minlength: 10,
                        maxlength: 10,
                        number: true,
                        required: true
                    },
                    email: {
                        email: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success105.hide();
                    error105.show();
                    App.scrollTo(error105, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    addnewreferencefun();
                  }
            });

    }

    var validateinteractioncategory = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form115 = $('#interactioncategory');
            var error115 = $('.alert-danger', form115);
            var success115 = $('.alert-success', form115);

            form115.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   interaction_category: {
                        required: true
                    },
                    categor_type:{
                        required : true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success115.hide();
                    error115.show();
                    App.scrollTo(error115, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });

    }

    var validateinteractionsubcategory = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form116 = $('#interactionsubcategory');
            var error116 = $('.alert-danger', form116);
            var success116 = $('.alert-success', form116);

            form116.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   interaction_subcategory: {
                        required: true
                    },
                    interaction_category: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success116.hide();
                    error116.show();
                    App.scrollTo(error116, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }
            });

    }
	
	var addrequest = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form105 = $('#addnewrequest');
            var error105 = $('.alert-danger', form105);
            var success105 = $('.alert-success', form105);
            
            //$.validator.addMethod("multiple1000",function(value, element) {
            //    return value % 1000 == 0
            //},"Amount Shold be multiple of 1000");

            form105.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
					amount:{required: true, digits: true, min: 1000},
					payment_mode:{required: true},
					bank_name:{required: true},
					cheque_no:{required: true},
					cheque_date:{required: true},
					contact_person:{required: true},
					contact_address:{required: true},
					pickup_date:{required: true},
					pickup_time:{required: true},
					courier_name:{required: true},
					transaction_id:{required: true},
					courier_tracking:{required: true}
                    //.calculatePay:{}
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success105.hide();
                    error105.show();
                    App.scrollTo(error105, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    
					addnewrequestfun();
                  }
            });

    }

    var addnewrequest2 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1052 = $('#addnewrequest2');
            var error1052 = $('.alert-danger', form1052);
            var success1052 = $('.alert-success', form1052);

            form1052.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    amount:{required: true, digits: true, min: 1000},
                    payment_mode:{required: true},
                    bank_name:{required: true},
                    cheque_no:{required: true},
                    cheque_date:{required: true},
                    contact_person:{required: true},
                    contact_address:{required: true},
                    pickup_date:{required: true},
                    pickup_time:{required: true},
                    courier_name:{required: true},
                    transaction_id:{required: true},
                    courier_tracking:{required: true}
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1052.hide();
                    error1052.show();
                    App.scrollTo(error1052, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    addnewrequestfun();
                  }
            });

    }


     var find_pincode = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form106 = $('#find_pincode');
            var error106 = $('.alert-danger', form106);
            var success106 = $('.alert-success', form106);

            form106.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   
                    collectionsubcentre: {
                        required: true
                    }

                 },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success106.hide();
                    error106.show();
                    App.scrollTo(error106, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                                   }
            });

    }
    var addsalesTarget = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form107 = $('#addsalesTarget');
            var error107 = $('.alert-danger', form107);
            var success107 = $('.alert-success', form107);

            form107.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   
                    number_of_application: {
                        required: true
                    },
                    start_date: {
                        required: true
                    },
                    end_date: {
                        required: true
                    }
                   

                 },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success107.hide();
                    error107.show();
                    App.scrollTo(error107, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                                   }
            });

    }

    var validatelead = function() {
            var form106 = $('#validateleadcreat');
            var error106 = $('.alert-danger', form106);
            var success106 = $('.alert-success', form106);

            form106.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                   name: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    landmark: {
                        required: true
                    },
                    pincode: {
                        minlength: 6,
                        maxlength:6,
                        number:true,
                    },
					mobile: {
                        minlength: 10,
                        maxlength: 10,
                        number: true,
                        required: true
                    },
                    email: {
                        email: true
                    },
                    emi_size: {
                        number: true
                    },
                    state:{
                        required: true, 
                    },
                    city:{
                        required: true, 
                    },
                    age:{
                        number: true,
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success106.hide();
                    error106.show();
                    App.scrollTo(error106, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    addnewreferencefun();
                  }
            });

    }


    var validateresetpassword = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form118 = $('#changepasswords');
            var error118 = $('.alert-danger', form118);
            var success118 = $('.alert-success', form118);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form118.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    currentpassword: {
                       required: true
                    },
                    newpassword: {
                       minlength: 6,
                       required: true
                    },
                    passwordconfirmation: {
                        minlength: 6,
                        equalTo: "#password",
                        required: true
                    }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success118.hide();
                    error118.show();
                    App.scrollTo(error118, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }

    var validategenerateurl = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form121 = $('.createshortenurl');
            var error121 = $('.alert-danger', form121);
            var success121 = $('.alert-success', form121);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form121.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    actual_url: {
                       required: true
                    }
                   
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success121.hide();
                    error121.show();
                    App.scrollTo(error121, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    form.submit();
                }

            });
    }

    var validatecustomerfile = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form152 = $('#validateupload');
            var error152 = $('.alert-danger', form152);
            var success152 = $('.alert-success', form152);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form152.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    attach: {
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success152.hide();
                    error152.show();
                    App.scrollTo(error152, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                   form.submit();
                }

            });
    }

     var validatepayment = function() {
        var form120 = $('#paymentmaster');
        var error120 = $('.alert-danger', form120);
        var success120 = $('.alert-success', form120);
        form120.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false, 
            ignore: ":hidden",
            rules: {
                mmp_txn: {
                    required: true
                },
                mer_txn:{
                    required: true
                },
                amt:{
                    required: true
                },
                date:{
                   required: true
                },
                f_code:{
                   required: true
                },
                udf9:{
                   required: true
                },
                payment_gateway:{
                   required: true
                }
            },
            errorPlacement: function (error, element) { 
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element); 
                }
            },
            invalidHandler: function (event,validator) { 
                success120.hide();
                error120.show();
                App.scrollTo(error120,-200);
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                funsubmitpayment();
            }
        });
    }

    var changerequestvalidation = function() {
        

            var form200 = $('#changerequest');
            var error200 = $('.alert-danger', form200);
            var success200 = $('.alert-success', form200);
            
            $.validator.addMethod("multiple1000",function(value, element) {
                return value % 1000 == 0
            },"Amount Shold be multiple of 1000");

            form200.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
                    contact_email: {
                        required: true,
                        email: true
                    },
                    applicant_name: {
                        required: true
                    },
                    contact_mobile: {
                        number: true,
                        required: true,
                        minlength: 10
                    },
                    applicant_dob:{
                        required: true
                    },
                    applicant_gender:{
                        required: true  
                    },
                    mailing_address:{
                        required: true
                    },
                    mailing_landmark:{
                        required: true  
                    },
                    mailing_state:{
                        required: true  
                    },
                    mailing_city:{
                        required: true   
                    },
                    mailing_district:{
                        required: true  
                    },
                    mailing_pincode:{
                        required: true  
                    },
                    amount:{
                        required:true,
                        multiple1000:true
                    },
                    fulfilment:{
                      required:true  
                    },
                    lead_source:{
                      required:true  
                    },
                    lead_source_other:{
                        required:function(element){
                            return $('#lead_source_oth').is(':checked')
                        }
                    },
                    amount:{required: true, digits: true, min: 1000},
                    payment_mode:{required: true},
                    bank_name:{required: true},
                    cheque_no:{required: true},
                    cheque_date:{required: true},
                    contact_person:{required: true},
                    contact_address:{required: true},
                    pickup_date:{required: true},
                    pickup_time:{required: true},
                    courier_name:{required: true},
                    transaction_id:{required: true},
                    courier_tracking:{required: true}
                },
                
               

                messages: { // custom messages for radio buttons and checkboxes
                   
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                   
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success200.hide();
                    //error200.show();
                    App.scrollTo(error200, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //form.submit();
                    validatedata();
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form200).change(function () {
                form200.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
    }

    var validatecharges = function() {
        var form201 = $('.charges');
        var error201 = $('.alert-danger', form201);
        var success201 = $('.alert-success', form201);
        form201.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false, 
            ignore: ":hidden",
            rules: {
                charge_heading: {
                    required: true
                },
                customer_id:{
                    required: true
                },
                amount:{
                    required: true,
                    number: true
                },
                remarks:{
                   required: true
                },
                status:{
                    required: true  
                }
            },
            errorPlacement: function (error, element) { 
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element); 
                }
            },
            invalidHandler: function (event,validator) { 
                success201.hide();
                error201.show();
                App.scrollTo(error201,-200);
            },
            highlight: function (element) { 
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                funsubmitpayment();
            }
        });
    }

    var foremarks = function(){
        var form202 = $('#frmforemarks');
        var error202 = $('.alert-danger', form202);
        var success202 = $('.alert-success', form202);
        form202.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                call_status: {
                    required: true
                },
                call_sub_status: {
                    required: true
                },
                verification_interaction: {
                    required: true
                }
            },
            errorPlacement: function (error, element){
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator){
                success202.hide();
                error202.show();
                App.scrollTo(error202, -200);
            },
            highlight: function (element){
               $(element)
                    .closest('.form-group').addClass('has-error');
            },

            unhighlight: function (element){
                $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }

        });
    }

    var fomeeting = function(){
        var form202 = $('#fomeeting');
        var error202 = $('.alert-danger', form202);
        var success202 = $('.alert-success', form202);
        form202.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: ":hidden",
            rules: {
                meeting: {
                    required: true,
                },
                remarks: {
                    required: true,
                },
                verification_interaction:{
                    required: true,
                },
                nireason:{
                    required: true,  
                },
                reasonother:{
                    required: true,  
                },
                amount:{
                    required: true, 
                    multiple1000:true,
                },
                paymentmode:{
                    required: true,
                },
                form_no:{
                    required: true,  
                },
                ecs:{
                    required: true,   
                },
                paydate:{
                    required: true,
                },
                hth:{
                    required: true,  
                },
                subcategory:{
                    required: true,  
                },
                transaction_id:{
                    required: true,  
                },
                chequeno:{
                    required:function(element){
                        return $('#remarks option:selected').val()=='saledone';
                    }
                },
                chequedate:{
                    required:function(element){
                        return $('#remarks option:selected').val()=='saledone';
                    }
                },
                bankname:{
                    required:function(element){
                        return $('#remarks option:selected').val()=='saledone';
                    }
                }
            },
            errorPlacement: function (error, element){
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator){
                success202.hide();
                error202.show();
                App.scrollTo(error202, -200);
            },
            highlight: function (element){
               $(element)
                    .closest('.form-group').addClass('has-error');
            },

            unhighlight: function (element){
                $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                callpost();
            }

        });
    }

    var vocupdate = function(){
        var form203 = $('#vocupdate');
        var error203 = $('.alert-danger', form203);
        var success203 = $('.alert-success', form203);
        form203.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: ":hidden",
            rules: {
                call_status: {
                    required: true,
                },
                contact:{
                    required: true,
                },
                call_sub_status: {
                    required: true,
                },
                verification_interaction: {
                    required: true,
                },
                appointment_date:{
                    required: true,  
                },
            },
            errorPlacement: function (error, element){
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator){
                success203.hide();
                error203.show();
                App.scrollTo(error203, -200);
            },
            highlight: function (element){
               $(element).closest('.form-group').addClass('has-error');
            },

            unhighlight: function (element){
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                form.submit();
            }

        });
    }
    
    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

	



    return {
        //main function to initiate the module
        init: function () {
			userCreate();
			userEdit();
			customerInteraction();
			customerRegistration();
			validateCall();
            validateLanguage();
			validatecollectioncentre();
            validatecollectionsubcentre();
            validatepincode();
            pincodecsv();
            validateemailtemplate();
            validatesmstemplate();
            find_pincode();
            addreference();
            addsalesTarget();
            addrequest();
            addnewrequest2();
            validatelead();
            validateresetpassword();
            validateinteractioncategory();
            validateinteractionsubcategory();
            validategenerateurl();
            validatecustomerfile();
            emailsmsvalidation();
            validatepayment();
            changerequestvalidation();
            validatecharges();
            editVerifier();
			spokupdate();
            foremarks();
            fomeeting();
            vocupdate();
        }
    };
}();