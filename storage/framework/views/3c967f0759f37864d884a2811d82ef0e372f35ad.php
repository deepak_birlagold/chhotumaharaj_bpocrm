<?php $__env->startSection('page_title'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumb'); ?>
<li>
    <a href="#">Add New Lead Source</a>
</li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<section role="main" class="content-body">
    <?php if($errors->any()): ?>
    <div class="alert alert-danger text-cente">
        <?php foreach($errors->all() as $error): ?>
        <p><?php echo e($error); ?></p>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="row">

        <div class="col-md-12">
            <div class="col-md-12">
                <form id="form" action="<?php echo e(url('edit_leadsourcedata')); ?>" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="_token" id="_tokensc" value="<?php echo e(csrf_token()); ?>">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Edit Lead Source</h2>
                        </header>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Edit Lead Source Name <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="hidden" name="leadsource_id" class="form-control" value="<?php echo $usersdata[0]['leadsource_id']; ?>" />
                                    <input type="text" name="leadsourcename" class="form-control" value="<?php echo $usersdata[0]['leadsource_name']; ?>" placeholder="Please Enter Value" required/>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <button type="submit" class="btn green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="<?php echo e(URL::to('lead_source')); ?>" class="btn btn-default" >Cancel</a>

                                </div>
                            </div>
                        </footer>
                    </section>
                </form>
            </div>
        </div>
    </div>
</section>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
<script src="<?php echo e(URL::asset('public/assets/javascripts/app.js')); ?>"></script>
    <script>
    jQuery(document).ready(function() { 
       App.init();
       $('.pagination li a').addClass('clk');
       $('#loading').hide();
    });
</script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>