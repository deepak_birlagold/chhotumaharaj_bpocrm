<?php $__env->startSection('page_title'); ?>
    Add New Distributor Lead
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
    <header class="page-header">
        <h2>Add Distributor Lead</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Add Distributor Lead</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-5">Distributor Lead</h2>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger text-center col-md-3">
                            <?php foreach($errors->all() as $error): ?>
                                <p><?php echo e($error); ?></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </header>
            <div class="panel-body">
                <form id="form" action="<?php echo e(url('lead-create')); ?>" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="_token" id="_tokensc" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="statename" value="" id="statename">
                     <input type="hidden" name="cityname" value="" id="cityname">
                     
                   
                        <div class="form-group">
                            <label class="col-md-3 control-label"> Name <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="name" class="form-control" placeholder="Please Enter Name"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Mobile <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="mobile" class="form-control" placeholder="Please Enter Mobile" pattern="[789][0-9]{9}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-6">                              

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input type="email" name="email" class="form-control" placeholder="Please Enter Email" autocomplete="off" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Gender</label>
                            <div class="col-md-6">
                                <select class="form-control" name="gender" id="gender">
                                    <option value=""> -- Select Gender --</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="not_to_say">Prefer Not to say</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">State <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" name="state" id="state">
                                    <option value="">Select State</option>
                                    <?php foreach($states as $key=>$val): ?>
                                    <option value="<?php echo e($val['id']); ?>"><?php echo e($val['state_name']); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">City <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" name="city" id="city">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Area <span class="required">*</span></label>
                            <div class="col-md-6">
                                 <input type="text" name="areaname" data-required="1" class="form-control" placeholder="Please Enter Area" value="<?php echo e(Request::old('areaname')); ?>" />
                                <!-- <select class="form-control" name="area" id="area">
                                    <option value="">Select Area</option>
                                </select> -->
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Address <span class="required">*</span></label>
                            <div class="col-md-6">
                                <textarea style="width:100%;" class="form-control" rows="3" name="address" data-error-container="#editor1_error" placeholder="Please Enter Address" ><?php echo e(Request::old('address')); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Landmark <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="landmark" data-required="1" class="form-control" placeholder="Please Enter Landmark" value="<?php echo e(Request::old('landmark')); ?>" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Current Profession </label>
                            <div class="col-md-6">
                                <select class="form-control" name="current_profession" id="current_profession">
                                    <option value="">Select profession</option>
                                    <?php foreach($currentprofession as $key=>$value): ?>
                                        <option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group row other_current_profession" style="display: none;">
                            <label class="col-md-3 control-label">Other profession <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="other_current_profession" id="other_current_profession" data-required="1" class="form-control" placeholder="Mention Other Current Profession" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Total Experience(in Year)</label>
                            <div class="col-md-6">
                                <select class="form-control" name="total_experience">
                                    <option value="">---</option>
                                    <?php for($i=0;$i<=30;$i++): ?>
                                        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                    <?php endfor; ?>
                                    <option value="31">31 and above</option>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label">Company Name</label>
                            <div class="col-md-6">
                                <input type="text" name="company_name" data-required="1" class="form-control" placeholder="Please Enter Company Name" value="<?php echo e(Request::old('company_name')); ?>" />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Current Income </label>
                            <div class="col-md-6">
                                <input type="text" name="income" data-required="1" class="form-control" placeholder="Please Enter Income" value="<?php echo e(Request::old('income')); ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Lead Source<span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" name="leadsourcevalue" id="leadsourcevalue">
                                    <option value="">Select</option>
                                    <?php foreach($leadsourcevalue as $key=>$val): ?> 
                                        <option value="<?php echo e($key); ?>"><?php echo e($val); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row other_lead_source" style="display:none">
                            <label class="col-md-3 control-label">Other Lead Source <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="other_lead_source" id="other_lead_source" data-required="1" class="form-control" placeholder="Mention Other Lead Source" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Industry Type </label>
                            <div class="col-md-6">
                                <input type="text" name="industry_type" data-required="1" class="form-control" placeholder="Please Enter Industry Type" value="<?php echo e(Request::old('industry_type')); ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">SubIndustry Type </label>
                            <div class="col-md-6">
                                <input type="text" name="subindustry_type" data-required="1" class="form-control" placeholder="Please Enter SubIndustry Type" value="<?php echo e(Request::old('subindustry_type')); ?>" />
                            </div>
                        </div>

                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <button type="submit" class="btn green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="<?php echo e(URL::to('lead-list')); ?>" class="btn btn-default" >Cancel</a>
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                </div>
                            </div>
                        </footer>
                    </section>
                </form>
                </div>
            </div>
        </section>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
    <script src="<?php echo e(URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('public/assets/javascripts/forms/form-validation.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('public/assets/javascripts/app.js')); ?>"></script>
    <script>
    jQuery(document).ready(function () {
        App.init();
        
        FormValidation.init();
        $('#loading').hide();
    });

    $("#state").change(function () {
        var stateID = $("#state").val();
        var token = $("#_tokensc").val();
        $('#statename').val($("#state option:selected").html());
        $("#city option").not(':first').remove();
        $("#area option").not(':first').remove();
        if (stateID) {
            $.ajax({
                url: "<?php echo e(route('getcitylist')); ?>",
                data: {'stateid': stateID, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){	
    		      $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#city").append(
                            $("<option></option>").text(obj.city_name).val(obj.id)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });

    $('#city').change(function(){
        $('#cityname').val($('#city option:selected').html());
        var token = $("#_tokensc").val();
        var cityId = $("#city").val();
        $("#area option").not(':first').remove();
        if (cityId) {
            $.ajax({
                url: "<?php echo e(route('getarealist')); ?>",
                data: {'cityid': cityId, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                  $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#area").append(
                            $("<option></option>").text(obj.area_name).val(obj.id)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });

    $("#area").change(function(){
        $('#areaname').val($('#area option:selected').html());
    });

    function formsubmit(obj){
        var flag = false;
        var mobile = $('input[name="mobile"]').val();
        $.ajax({
            url: "<?php echo e(route('check-mobile')); ?>",
            data: {'mobile': mobile},
            type: 'get',
            async: true,
            cache: false,
            clearForm: false,
            beforeSend:function(){  
              $('#loading').show();
            },
            success:function (response) {
                if(response== undefined){
                    var check = function(){
                        callback();
                    };
                }
                if(response==1){
                    alert("Mobile No Already Exists!!!");
                }else{
                    obj.submit();
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
        });
        return false;
    }

    function callback(){
        return check();
    }

    $('#current_profession').change(function() {
        if($(this).val() == '0') { 
            $('.other_current_profession').show();
        } else {
            $('#other_current_profession').val('');
            $('.other_current_profession').hide();
        }
    });

    $('#leadsourcevalue').change(function() {
        if($(this).val() == '0') { 
            $('.other_lead_source').show();
        } else {
            $('#other_lead_source').val('');
            $('.other_lead_source').hide();
        }
    });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>