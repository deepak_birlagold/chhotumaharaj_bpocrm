<?php $__env->startSection('page_title'); ?>
    Distributor Not Interested
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
    <header class="page-header">
        <h2>Distributor Not Interested</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Distributor Not Interested</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-4">Distributor Not Interested</h2>
                    <div class="alert alert-success col-md-5 message" style="padding:7px;margin-bottom: -8px;display:none;"></div>
                </div>
            </header>
            <div class="panel-body">
                <form class="navbar-form" role="search" action="<?php echo e(route('recevied-list')); ?>" name="frm" id="frm">
                    <div class="row">
                        <label class="col-md-1 control-label">Search</label> 
                        <div class="col-md-3 form-group">
                            <input type="text" name='q' value="<?php echo e($q); ?>" class="form-control" id="q">
                        </div>
                       <div class="form-group col-md-3">
                            <select class="form-control" name="option" id="option">
                                <option value=""> -- Select Status -- </option>
                                <option value="ID" <?php if($option=='ID'): ?> selected <?php endif; ?>>ID</option>
                                <option value="Name" <?php if($option=='Name'): ?> selected <?php endif; ?>>Name</option>
                                <option value="Email" <?php if($option=='Email'): ?>selected <?php endif; ?>>Email</option>                           
                                <option value="Mobile" <?php if($option=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                            </select>   
                        </div>
                        <div class="form-group col-md-3">
                            <input type="submit" class="btn" value="Search">
                            <a href="<?php echo e(route('recevied-list')); ?>" class="btn btn-primary" >Clear</a>
                        </div>
                    </div>													
                </form>
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Tele Caller</th>
                        </tr>
                    </thead>
                    <tbody>                  
                        <?php if(count($leadsourcevalue) > 0): ?>
                            <?php foreach($leadsourcevalue as $key => $val): ?>
                                <tr data-id="<?php echo e($key); ?>">
                                    <td> 
                                        <?php echo e($val->leadid); ?>

                                    </td>
                                    <td>
                                        <?php echo e($val->leadname); ?>

                                    </td>
                                    <td>
                                        <?php echo e($val->leadmobile); ?>

                                    </td>
                                    <td>
                                        <?php echo e($val->leademail); ?>

                                    </td>
                                    <td>
                                        <?php echo e($val->username); ?>

                                    </td>
                                    
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <div class ="pull-right" style="margin-bottom:20px">
                    <?php echo $leadsourcevalue->appends(['q'=>(isset($q)?$q:""),'option'=>(isset($option)?$option:"")])->render(); ?>

                </div>
            </div>
        </section>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
    <script src="<?php echo e(URL::to('/public/assets/javascripts/app.js')); ?>"></script>
    <script src="<?php echo e(URL::to('/public/assets/bootbox/bootbox.min.js')); ?>"></script>
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide(); 
        });
        
        
        $('#frm').submit(function(){
            $('#loading').show();
            var search = $('#q').val()=="" ? $('#q').focus() : true;
            var option = $('#option option:selected').val()=="" ? $('#option').focus() : true;
            if(search==true && option==true){
                return true;
            }
            $('#loading').hide();
            return false;
        });

</script>

<?php $__env->stopSection(); ?>			
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>