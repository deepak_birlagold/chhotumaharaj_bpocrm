<?php $__env->startSection('page_title'); ?>
    Converted List 
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
    <header class="page-header">
        <h2>Distributor Converted List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Distributor Converted List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Distributor Converted List</h2>
            </header>
        </section>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered"> 
                <tbody>
                    <tr>
                        <td>
                            <button type="button" id="clk" class="btn btn-primary">Excel</button>
                        </td>
                    </tr>
                </tbody>
            </table><br>
            <!-- <div class="col-md-12">
                <div class="row">
                    <form class="form-horizontal">
                        <div class="col-md-1">From Date</div>
                        <div class="col-md-2">  
                            <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="<?php echo e(isset($q)?$q:""); ?>">
                        </div>
                        <div class="col-md-1">From Da</div>
                        <div class="col-md-2">  
                            <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="<?php echo e(isset($q)?$q:""); ?>">
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            
                        </div>
                    </form>
                </div>
            </div> -->
            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                <thead>
                    <tr>
                        <th>Lead ID</th>
                        <th>Created Date</th>
                        <th>Request Date</th>
                        <th>Conversion Date</th>
                        <th>Verified Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>                      
                        <th>Tell Caller</th>
                        <th>Verification Date</th>
                        <th>Emi Amount</th>
                        <th>Gram Allocated</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($result) > 0): ?>
                        <?php foreach($result as $key => $val): ?>
                            <tr>
                                <td>
                                    <?php echo e($val->reference_id); ?>

                                </td>
                                 <td>
                                    <?php echo e($val->created_date); ?>

                                </td>
                                <td>
                                    <?php echo e($val->requestdate); ?>

                                </td>
                                <td>
                                    <?php echo e($val->conversiondate); ?>

                                </td>
                                <td>
                                    <?php echo e($val->verifieddate); ?>

                                </td>
                                <td>
                                    <?php echo e($val->name); ?>

                                </td>   
                                <td>
                                    <?php echo e($val->email); ?>

                                </td>
                                <td>
                                    <?php echo e($val->mobile); ?>

                                </td>
                                <td>
                                    <?php echo e($val->tellcallername); ?>

                                </td>
                                <td>
                                    <?php echo e($val->verified_date); ?>

                                </td>
                                <td>
                                    <?php echo e($val->emiamount); ?>

                                </td>
                                <td>
                                    <?php echo e($val->gram); ?>

                                </td>

                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="8" align="center">
                                Not Data Found
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <div class ="pull-right" style="margin-bottom:20px">
                <?php echo $result->render(); ?>

            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#loading').hide();
        });

        $('#clk').click(function(){
            window.location.href="<?php echo e(route('convertedlistexport')); ?>";
        })
    </script>
<?php $__env->stopSection(); ?>			
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>