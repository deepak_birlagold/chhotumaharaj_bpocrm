<?php $__env->startSection('page_title'); ?>
   Distributor Lead List 
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
    <header class="page-header">
        <h2>Distributor Lead List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Distributor Lead</span></li>
                <li><span>Distributor Lead List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Distributor Lead List</h2>
            </header>
            <div class="panel-body">
                <div class="col-md-12">
                   <form class="navbar-form" role="search" action="<?php echo e(route('lead-list')); ?>" name="frm" id="frm">
                        <div class="row">
                            <label class="col-md-1 control-label">Search</label> 
                            <div class="col-md-3 form-group">
                                <input type="text" name='q' value="<?php echo e($q); ?>" class="form-control" id="q">
                            </div>
                           <div class="form-group col-md-3">
                                <select class="form-control" name="option" id="option">
                                    <option value=""> -- Select Status -- </option>
                                    <option value="ID" <?php if($option=='ID'): ?> selected <?php endif; ?>>ID</option>
                                    <option value="Name" <?php if($option=='Name'): ?> selected <?php endif; ?>>Name</option>
                                    <option value="Email" <?php if($option=='Email'): ?>selected <?php endif; ?>>Email</option>                           
                                    <option value="Mobile" <?php if($option=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                </select>   
                            </div>
                            <div class="form-group col-md-3">
                                <input type="submit" class="btn" value="Search">
                                <a href="<?php echo e(route('lead-list')); ?>" class="btn btn-primary" >Clear</a>
                            </div>
                        </div>                                                  
                    </form>
                </div>
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Tell Caller</th>
                            <th>Call Status</th>
                            <th>Call Sub Status</th>
                            <th> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($result) > 0): ?>
                            <?php $call_status = callstatus();
                            $sub_call_status= callsubstatus(); ?>
                            <?php foreach($result as $key => $val): ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo e(URL::to('lead-details',$val->reference_id)); ?>" class="on-default edit-row"><?php echo e($val->reference_id); ?></a>
                                    </td>
                                    <td>
                                        <?php echo empty($val->name) ? "-" : $val->name; ?>
                                    </td>
                                    <td>
                                        <?php echo $val->email; ?> 
                                    </td>
                                    <td class="center">
                                        <?php echo $val->mobile; ?> 
                                    </td>
                                    <td class="center">
                                        <?php echo $val->state_name; ?>  
                                    </td>
                                    <td class="center">
                                        <?php echo $val->city_name; ?>  
                                    </td>
                                    <td class="center">
                                        <?php echo $val->username; ?>  
                                    </td>
                                    <td class="center">
                                        <?php echo e(isset($call_status[$val->call_status_id]) ? $call_status[$val->call_status_id] : ""); ?>

                                    </td>
                                     <td class="center">
                                        <?php echo e(isset($sub_call_status[$val->call_substatus_id]) ? $sub_call_status[$val->call_substatus_id] : ""); ?>

                                    </td>
                                    <td class="center hidden-phone"> 
                                        <a href="<?php echo e(URL::to('lead-details',$val->reference_id)); ?>" class="on-default edit-row"><i class="fa fa-sitemap"></i></a><!--  /  
                                    <a href="<?php echo e(URL::route('leadrequest',$val->reference_id)); ?>" class="on-default edit-row"><i class="fa fa-info-circle"></i></a> -->
                                </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <div class ="pull-right" style="margin-bottom:20px">
                    <?php echo $result->appends(['q'=>$q,'option'=>$option])->render(); ?>

                </div>
            </div>
        </section>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide();
        });

        $('#frm').submit(function(){
            $('#loading').show();
            var search = $('#q').val()=="" ? $('#q').focus() : true;
            var option = $('#option option:selected').val()=="" ? $('#option').focus() : true;
            if(search==true && option==true){
                return true;
            }
            $('#loading').hide();
            return false;
        });
    </script>
<?php $__env->stopSection(); ?>			
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>