
<?php $__env->startSection('page_title'); ?>
    Call Back List 
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
    <header class="page-header">
        <h2>Call Back List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Call Back</span></li>
                <li><span>Call Back List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Call Back List</h2>
            </header>
            <div class="panel-body">
                <div class="col-md-12>"
                    
                </div>
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Tele Caller</th>
                            <th>CallBack</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($result)>0): ?>
                            <?php foreach($result as $key => $val): ?>
                                <tr>
                                    <td><?php echo e($val->reference_id); ?></td>
                                    <td><?php echo e($val->leadname); ?></td>
                                    <td><?php echo e($val->leademail); ?></td>
                                    <td><?php echo e($val->leadmobile); ?></td>
                                    <td><?php echo e($val->username); ?></td>
                                    <td><?php echo e($val->callbacktime); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class ="pull-right" style="margin-bottom:20px">
                <?php echo $result->render(); ?>

            </div>
        </section>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide();
        });
    </script>
<?php $__env->stopSection(); ?>         
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>