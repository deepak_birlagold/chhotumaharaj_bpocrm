<?php $__env->startSection('page_title'); ?>
    Dashboard
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_style_top'); ?>
    <header class="page-header">
        <h2>Dashboard</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section role="main" class="content-body">
        <div class="backgroundchanges" style="border-radius: 15px;background-color: rgba(255, 255, 255, 0.7); padding:20px;">
            <h2>Distributor</h2>
            <div class="row">
                <div class="col-md-4">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                        <i class="fa fa-users"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Distributor Lead</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($lead); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('leadlist_report')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div> 
                <div class="col-md-4">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Distributor CallBack</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($distributorcallback); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                     <a  href="<?php echo e(URL::route('callback','distributor')); ?>" class="text-muted text-uppercase">(View)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                        <i class="fa fa-thumbs-o-down"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Distributor Not intersted</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($notintersted); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                     <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('notinterestedlist')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                        <i class="fa fa-thumbs-o-up"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Distributor Lead Requested</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($leadreceive); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                     <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('requestlist')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fa fa-check-circle-o"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Distributor Lead Converted</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($converted); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('convertedlist')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="backgroundchanges" style="border-radius: 15px; background-color: rgba(255, 255, 255, 0.7); padding:20px;margin-top: 15px;">
            <h2>Franchise</h2>
            <div class="row">
                <div class="col-md-4">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                         <i class="fa fa-users"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Franchise Lead</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($leadfranchise); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('franchiseleadlist_report')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div> 
                <div class="col-md-4">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                         <i class="fa fa-phone"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Franchise CallBack</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($franchisecallback); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                     <a  href="<?php echo e(URL::route('callback','franchise')); ?>" class="text-muted text-uppercase">(View)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                        <i class="fa fa-thumbs-o-down"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Franchise Not intersted</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($notinterstedfranchise); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                     <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('notinterestedfranchiselist')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-success">
                                        <i class="fa fa-thumbs-o-up"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Franchise Lead Requested</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($leadreceivefranchise); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                     <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('franchiserequestlist')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-featured-left panel-featured-success">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fa fa-check-circle-o"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                     <div class="summary">
                                        <h4 class="title">Franchise Lead Converted</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo e($franchiseconverted); ?></strong>
                                            <span class="text-primary"></span>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <?php if(Auth::user()->role!="Tele Caller"): ?>
                                            <a  href="<?php echo e(URL::route('franchiseconvertedlist')); ?>" class="text-muted text-uppercase">(View)</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_level_script_bottom'); ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('#loading').hide();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>