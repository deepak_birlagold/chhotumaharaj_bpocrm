<!DOCTYPE html>
<html class="fixed" id="mainhtml">
	<head>
		<meta charset="UTF-8">
		<title>Chhotu Maharaj - BPO-CRM | <?php echo $__env->yieldContent('page_title'); ?></title>
		<meta name="keywords" content="Cherish Gold | Birla Gold" />
		<meta name="description" content="Cherish Gold | BPO-CRM">
		<meta name="author" content="Sukhsagar">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" href="<?php echo e(URL::asset('public/assets/favicon.ico')); ?>" type="image/x-icon" />
		 <link href="<?php echo e(URL::asset('public/assets/stylesheets/googleapis.css')); ?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/bootstrap/css/bootstrap.css')); ?>"/>
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/font-awesome/css/font-awesome.css')); ?>"/>
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/magnific-popup/magnific-popup.css')); ?>"/>
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css')); ?>" />
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')); ?>" />
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/morris/morris.css')); ?>" />
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/select2/select2.css')); ?>" />
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')); ?>" />
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/stylesheets/theme.css')); ?>"/>
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/stylesheets/skins/default.css')); ?>" />
		<link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/stylesheets/theme-custom.css')); ?>">
		<script src="<?php echo e(URL::asset('public/assets/vendor/modernizr/modernizr.js')); ?>"></script>
		<?php echo $__env->yieldContent('page_level_style_top'); ?>
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="loading" id="loading"> 
            <img class="loading-image" id="loading-image" src="<?php echo e(URL::asset('public/assets/images/ajax-loading.gif')); ?>" alt="Loading..." />
        </div>
	</head>
	<body>
		<section class="body">
			<div class="inner-wrapper">
				<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php echo $__env->yieldContent('content'); ?>		
			</div>
		</section>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<script src="<?php echo e(URL::asset('public/assets/javascripts/jquery.min.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/bootstrap/js/bootstrap.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/nanoscroller/nanoscroller.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/magnific-popup/magnific-popup.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-placeholder/jquery.placeholder.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js')); ?>"></script>
		<!--script src="<?php echo e(URL::asset('public/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js')); ?>"></script-->
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-appear/jquery.appear.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')); ?>"></script>
		<!--script src="<?php echo e(URL::asset('public/assets/vendor/jquery-easypiechart/jquery.easypiechart.js')); ?>"></script-->
		<!--<script src="<?php echo e(URL::asset('public/assets/vendor/flot/jquery.flot.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/flot-tooltip/jquery.flot.tooltip.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/flot/jquery.flot.pie.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/flot/jquery.flot.categories.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/flot/jquery.flot.resize.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-sparkline/jquery.sparkline.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/raphael/raphael.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/morris/morris.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/gauge/gauge.js')); ?>"></script>-->
		<script src="<?php echo e(URL::asset('public/assets/vendor/snap-svg/snap.svg.js')); ?>"></script>
		<!--<script src="<?php echo e(URL::asset('public/assets/vendor/liquid-meter/liquid.meter.js')); ?>"></script>-->
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/vendor/ios7-switch/ios7-switch.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/javascripts/theme.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/javascripts/theme.custom.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/javascripts/theme.init.js')); ?>"></script>
		<script src="<?php echo e(URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')); ?>"></script>
		<?php echo $__env->yieldContent('page_level_script_bottom'); ?>
	</body>
</html>	