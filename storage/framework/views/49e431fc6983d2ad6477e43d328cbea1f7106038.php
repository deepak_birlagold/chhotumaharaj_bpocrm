<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            Chhotu Maharaj
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                     <li <?php if($controller=="dashboard"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                        <a href="<?php echo e(URL::to('dashboard')); ?>">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard </span>
                        </a>
                    </li>
                    <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="user"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Users</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="user-list"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('userlist')); ?>">
                                        User List
                                    </a>
                                </li>
                                <li <?php if($action=="add-user"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::to('add_user_form')); ?>">
                                        Add User
                                    </a>
                                </li>
                               
                            </ul>
                        </li>
                    <?php endif; ?>

                    <li <?php if($controller=="lead"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                        <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Distributor Lead</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="assign-lead-list"): ?> class="nav-active" <?php endif; ?>>
                                <a href="<?php echo e(URL::route('lead-list')); ?>">Distributor Lead List</a>
                            </li> 
                            <li <?php if($action=="leadadd"): ?> class="nav-active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('lead')); ?>">Add Distributor Lead</a>
                            </li>
                        </ul>
                    </li>
                    <li <?php if($controller=="franchise"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                        <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Franchise Lead</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="franchsielead-list"): ?> class="nav-active" <?php endif; ?>>
                                <a href="<?php echo e(URL::route('franchise-list')); ?>">Franchise Lead List</a>
                            </li> 
                            <li <?php if($action=="franchiseadd"): ?> class="nav-active" <?php endif; ?>>
                                <a href="<?php echo e(URL::route('addfranchise')); ?>">Add Franchise Lead</a>
                            </li>
                        </ul>
                    </li>

                    <?php if(Auth::user()->role =="admin"): ?>
                    <!-- <li <?php if($controller=="fc"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-align-left" aria-hidden="true"></i>
                            <span> Lms </span>
                        </a>
                        <ul class="nav nav-children">                            
                            <li class="nav-parent">
                                <a>Lead History</a>
                                <ul class="nav nav-children">

                                    <li <?php if($action=="rejectedlist"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('rm-rejected-list')); ?>">
                                            Rejected List
                                        </a>
                                    </li>
                                    <li <?php if($action=="acceptedlist"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('rm-accept-list')); ?>">
                                            Accepted List
                                        </a>
                                    </li>

                                    <li <?php if($action=="fofltsale"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('fo-ftl-sale')); ?>">
                                            Sale History
                                        </a>
                                    </li>  

                                    <li <?php if($action=="lmsreport"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('lms-report')); ?>">
                                            Lms Report
                                        </a>
                                    </li>   
                                </ul>
                            </li>

                            <li class="nav-parent">
                                <a>SPOC Management</a>
                                <ul class="nav nav-children">                                  
                                    <li <?php if($action=="spokField"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('spok-list')); ?>">
                                            Assign Field
                                        </a>
                                    </li>                                    
                                </ul>
                            </li>   


                            <li <?php if($action=="qualityverifyList"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('quality-list')); ?>">
                                    Verify Lead
                                </a>
                            </li>
                            <li <?php if($action=="voclist"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('voc-list')); ?>">
                                    VOC List 
                                </a>
                            </li>




                            <li class="nav-parent">
                                <a>Ftl Details</a>
                                <ul class="nav nav-children">                                  
                                    <li <?php if($action=="ftllist"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('ftl-list')); ?>">
                                            Assign FO 
                                        </a>
                                    </li>
                                    <li <?php if($action=="ftlassigned"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('ftl-assigned-list')); ?>">
                                            Assigned FO 
                                        </a>
                                    </li>                                   
                                </ul>
                            </li> 

                            <li class="nav-parent">
                                <a>Fo Details</a>
                                <ul class="nav nav-children">                                  
                                    <li <?php if($action=="folist"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('fo-list')); ?>">
                                            Lead Acceptance 
                                        </a>
                                    </li>
                                    <li <?php if($action=="fomeeting"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('fo-meeting-list')); ?>">
                                            Lead Meeting 
                                        </a>
                                    </li>                                  
                                </ul>
                            </li> 

                            <li class="nav-parent">
                                <a>Tl Details</a>
                                <ul class="nav nav-children">                                  
                                    <li <?php if($action=="tlassign-lead-list"): ?> class="nav-active" <?php endif; ?>>
                                         <a href="<?php echo e(URL::route('tllead-list')); ?>">
                                            TL Verify 
                                        </a>
                                    </li>                                  
                                </ul>
                            </li> 

                        </ul>
                    </li> -->

                    <?php endif; ?> 




                    <?php if(Auth::user()->role =="admin"): ?>
                    <!-- <li <?php if($controller=="fc"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Lead Quality Assign t</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="assignquality"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('leadQualityList')); ?>">
                                    Assign Quality Lead
                                </a>
                            </li>  

                        </ul> -->
                    </li>
                    <?php endif; ?> 

                   <!--  <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="leadsource"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Lead Source</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="sourceadd"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('lead_source')); ?>">
                                       Add Lead Source 
                                    </a>
                                </li>
                               
                            </ul>
                        </li>
                    <?php endif; ?> -->

                    <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="unassignedlead"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Unassigned Lead</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="unassignedlead-list"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('unassigned-lead')); ?>">
                                       Distributor Unassigned
                                    </a>
                                </li>
                                <li <?php if($action=="unassignedfranchiseleadlist"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('unassigned-franchiselead')); ?>">
                                       Franchise Unassigned
                                    </a>
                                </li>
                               
                            </ul>
                        </li>
                    <?php endif; ?>


                    <?php if(Auth::user()->role=="Team Lead"): ?>
                   <!--  <li <?php if($controller=="unassignedlead"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="tlassign-lead-list"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('tllead-list')); ?>">
                                    TL Verify 
                                </a>
                            </li>

                        </ul>
                    </li> -->
                    <?php endif; ?>    




                    <?php if(Auth::user()->role=="Verifier Head"): ?>
                    <li <?php if($controller=="verifierList"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="qualityverifyList"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('quality-list')); ?>">
                                    Verify Lead
                                </a>

                            </li>
                            <li <?php if($action=="verifierList"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('verifier-list')); ?>">
                                    Assign Lead
                                </a>

                            </li>
                        </ul>
                    </li>
                    <?php endif; ?> 




                    <?php if(Auth::user()->role=="Verifier"): ?>
                    <li <?php if($controller=="qualityverifyList"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>

                        <ul class="nav nav-children">
                            <li <?php if($action=="qualityverifyList"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('quality-list')); ?>">
                                    Verify Lead
                                </a>

                            </li>                            
                        </ul>
                    </li>
                    <?php endif; ?> 

                    <?php if(Auth::user()->role=="Spoc"): ?>
                    <li <?php if($controller=="spokField"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>

                        <ul class="nav nav-children">
                            <li <?php if($action=="spokField"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('spok-list')); ?>">
                                    Assign Lead
                                </a>

                            </li>                            
                        </ul>
                    </li>
                    <?php endif; ?> 



                    <?php if(Auth::user()->role=="FTL"): ?>
                    <li <?php if($controller=="ftl"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="ftllist"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('ftl-list')); ?>">
                                    LMS FTL Assign FO 
                                </a>
                            </li>
                            <li <?php if($action=="ftlassigned"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('ftl-assigned-list')); ?>">
                                    LMS FTL Assigned FO 
                                </a>
                            </li>

                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(Auth::user()->role ==  "Voc"): ?>
                    <li <?php if($controller=="ftl"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="voclist"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('voc-list')); ?>">
                                    VOC List 
                                </a>
                            </li>
                            <li <?php if($action=="vocnotinterested"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('voc-notinterested')); ?>">
                                    VOC Not Interested 
                                </a>
                            </li>

                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(in_array(Auth::user()->role,array('FO','Admin'))): ?>
                    <li <?php if($controller=="FO"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="folist"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('fo-list')); ?>">
                                    LMS FO Lead Acceptance 
                                </a>
                            </li>
                            <li <?php if($action=="fomeeting"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('fo-meeting-list')); ?>">
                                    LMS FO Lead Meeting 
                                </a>
                            </li>

                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if(Auth::user()->role!=="Tele Caller"): ?>
                       <!--  <li <?php if($controller=="verifylink"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Mass Link</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="linkverification"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('verifylink')); ?>">
                                       Mass Link 
                                    </a>
                                </li>
                               
                            </ul>
                        </li> -->
                    <?php endif; ?>

                  <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="notinterested"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Not Interested</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="notinterestedlist"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('not-interestedlist')); ?>">
                                       Distributor NI
                                    </a>
                                </li>
                                <li <?php if($action=="notinterestedfranchiselist"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('not-franchiseinterestedlist')); ?>">
                                       Franchise NI
                                    </a>
                                </li>
                               <!--  <li <?php if($action=="not-interesteddetails"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('not-interesteddetails')); ?>">
                                      Not Interested
                                    </a>
                                </li>
                                <li <?php if($action=="recevied-list"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('recevied-list')); ?>">
                                      Recevied
                                    </a>
                                </li> -->
                               
                            </ul>
                        </li>
                    <?php endif; ?>

                    <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="reportc"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                             <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span> Change Rm</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="leadchangerm"): ?> class="nav-active" <?php endif; ?>>
                                     <a href="<?php echo e(URL::route('leadchangerm')); ?>">
                                        Distributor Rm
                                    </a>
                                </li>
                                <li <?php if($action=="franchiseleadchangerm"): ?> class="nav-active" <?php endif; ?>>
                                     <a href="<?php echo e(URL::route('franchiseleadchangerm')); ?>">
                                        Franchise Rm
                                    </a>
                                </li>

                            </ul>
                        </li> 
                    <?php endif; ?>

                    <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="report"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Distributor Report</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="lead"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::to('leadlist-report')); ?>">
                                        Distributor Lead List
                                    </a>
                                </li>
                                 <li <?php if($action=="converted"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('convertedlist')); ?>">
                                        Distributor Converted
                                    </a>
                                </li>
                               <li <?php if($action=="request"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('requestlist')); ?>">
                                        Distributor Request
                                    </a>
                                </li>
                                <!--  <li <?php if($action=="verification"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('verificationlist')); ?>">
                                        Verification List
                                    </a>
                                </li> -->
                            <li <?php if($action=="notinterested"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('notinterestedlist')); ?>">
                                        Not Interested Distributor
                                    </a>
                                </li> 
                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php if(Auth::user()->role!=="Tele Caller"): ?>
                        <li <?php if($controller=="franchisereport"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                            <a>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Franchise Report</span>
                            </a>
                            <ul class="nav nav-children">
                                <li <?php if($action=="franchiselead"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::to('franchiseleadlist-report')); ?>">
                                        Franchise Lead List
                                    </a>
                                </li>
                                 <li <?php if($action=="franchiseconverted"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('franchiseconvertedlist')); ?>">
                                        Franchise Converted
                                    </a>
                                </li>
                               <li <?php if($action=="franchiserequest"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('franchiserequestlist')); ?>">
                                        Franchise Request
                                    </a>
                                </li>
                                <!--  <li <?php if($action=="verification"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('verificationlist')); ?>">
                                        Verification List
                                    </a>
                                </li> -->
                            <li <?php if($action=="notinterestedfranchise"): ?> class="nav-active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::route('notinterestedfranchiselist')); ?>">
                                        Not Interested Franchise
                                    </a>
                                </li> 
                            </ul>
                        </li>
                    <?php endif; ?>

                <?php if(Auth::user()->role=="Team Lead"): ?>
                    <!-- <li <?php if($controller=="reportc"): ?> class="nav-parent nav-expanded nav-active" <?php else: ?> class="nav-parent"  <?php endif; ?>>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span> Lms </span>
                        </a>
                        <ul class="nav nav-children">
                            <li <?php if($action=="assign-tllead-list"): ?> class="nav-active" <?php endif; ?>>
                                 <a href="<?php echo e(URL::route('tllead-list')); ?>">Lead List</a>
                            </li> 

                        </ul>
                    </li> -->
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
    </div>
</aside>