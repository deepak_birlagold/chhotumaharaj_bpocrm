<!DOCTYPE html>
<html class="fixed" id="mainhtml">
    <head>
        <meta charset="UTF-8">

        <title>Dashboard | Chhotu Maharaj - BPO-CRM</title>
        <meta name="keywords" content="Cherish Gold | Birla Gold" />
        <meta name="description" content="Cherish Gold | BPO-CRM">
        <meta name="author" content="Sukhsagar">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="<?php echo e(URL::asset('public/assets/favicon.ico')); ?>" type="image/x-icon" />
        <link href="<?php echo e(URL::asset('public/assets/stylesheets/googleapis.css')); ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/bootstrap/css/bootstrap.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/font-awesome/css/font-awesome.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css')); ?>" />
        
        <link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/stylesheets/theme.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/stylesheets/skins/default.css')); ?>" />
        <link rel="stylesheet" href="<?php echo e(URL::asset('public/assets/stylesheets/theme-custom.css')); ?>">
        <div class="loading" id="loading"> 
            <img class="loading-image" id="loading-image" src="<?php echo e(URL::asset('public/assets/images/ajax-loading.gif')); ?>" alt="Loading..." />
        </div>
    </head>
    <body>
        <section class="body-sign">
            <div align="center" class="center-sign">
                <a class="logo">
                    <img src="<?php echo e(asset('public/assets/images/logo/logo-1.png')); ?>" width="200" height="200" style="margin-bottom: 10px;" >
                </a>
                <div class="panel panel-sign">
                    <div class="panel-body">
                        <?php if(Session::has('msg')): ?>
                        <div class="alert alert-danger text-cente">
                            <span>
                                <h5> <?php echo e(Session::get('msg')); ?></h5>
                            </span>
                        </div>  
                        <?php endif; ?>
                        <form class="login-form" action="<?php echo e(route('postlogin')); ?>" id="loginform" method="post" name="loginform">
                            <input type="hidden" name="_token" value="<?php echo e(Session::token()); ?>">

                            <div class="form-group">
                                <label class="pull-left">Username</label>
                                <div class="input-group input-group-icon">
                                    <input name="email" type="text" class="form-control input-lg" placeholder="Login ID" autocomplete="off"/>
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="pull-left">Password</label>
                                <div class="input-group input-group-icon">
                                    <input name="password" type="password" class="form-control input-lg" placeholder="Password"/>
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">

                                </div>
                                <div class="col-sm-4 text-right">
                                    <button type="submit" class="btn btn-primary1 hidden-xs">Sign In</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2018. All Rights Reserved.</p>
                </div>
        </section>
        <script src="<?php echo e(URL::asset('public/assets/javascripts/jquery.min.js')); ?>"></script>
       
        <script src="<?php echo e(URL::asset('public/assets/vendor/bootstrap/js/bootstrap.js')); ?>"></script>
        
        
        <script src="<?php echo e(URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')); ?>"></script>
        <script src="<?php echo e(URL::asset('public/assets/javascripts/forms/form-validation.js')); ?>"></script>
        <script src="<?php echo e(URL::asset('public/assets/javascripts/app.js')); ?>"></script>
        <script>
            jQuery(document).ready(function () {
                App.init();
                FormValidation.init();
                $('#loading').hide();

            });
            function formsubmit(form){
                $('#loading').show();
                form.submit();
            }
        </script>
    </body>
</html> 